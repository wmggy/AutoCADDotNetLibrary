# 使用 Attribute 简化逻辑

### IExtensionApplication

```c#
public class Init : IExtensionApplication
{
    public void Initialize()
    {
        ExtensionApplication.Initialize(Assembly.GetExecutingAssembly());
    }

    public void Terminate()
    {
        ExtensionApplication.Terminate(Assembly.GetExecutingAssembly());
    }

    [Initialize]//ExtensionApplication.Initialize()执行这个方法
    public void hello()
    {
        Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("\nhello word");
    }
}
```

`ExtensionApplication.Initialize()`执行所有带有`[Initialize]`或其派生类的方法。

- `[Initialize]`：初始化时，执行的方法。
- `[ComMenuInitialize("AutoCAD 经典")]`：初始化时，执行制作 COM 的菜单的方法，并在工作空间变化时，再次执行函数。
- `[RibbonInitialize("草图与注释")]`：初始化时，执行制作 Ribbon 的菜单的方法，并在工作空间变化时，再次执行函数。
- `[IdleInitialize]`：初始化时，延迟执行函数。目的是解决刚启动 AutoCAD 时，无法执行`cuiload`等命令的问题。

### Com 菜单，快捷菜单，CUI，Ribbon

由于类封装的缘故，相关命令会放在一个类型中。显示 UI 时，一般也会并列出现。因此，可以根据类型的反射信息去生成 UI。

根据以上的的思路，应用在了以下位置：

- `[AcadPopupMenuItem]`：记录`COM菜单`的信息。示例详见[ComMenu.cs](../test/Test/TestCode/UI/ComMenu.cs)。
- `[ContextMenuItem]`：记录`快捷菜单`的信息。示例详见[ContextMenu.cs](../test/Test/TestCode/UI/ContextMenu.cs)。
- `[MenuMacro]`：记录`CUI的宏`的信息。示例详见[CUI.cs](../test/Test/TestCode/UI/CUI.cs)。
- `[PopMenuItem]`：记录`CUI菜单`的信息。
- `[RibbonButton]`：记录`Ribbon`的信息。示例详见[MyRibbonTab.xaml](../test/Test/Test2013/MyRibbonTab.xaml)。

在生成对应的 UI 时，会通过**类型**反射得到`CommandMethodAttribute`的`命令名称`和以上信息，以此简化生成 UI 的代码。

#### Ribbon

`Ribbon`是 WPF 写的，可以像 WPF 的`Window`一样使用 xaml 语言。示例详见[MyRibbonTab.xaml](../test/Test/Test2013/MyRibbonTab.xaml)。

> 解决在`Visual Studio`中打开项目的 xaml 会报错的问题，选项->文本编辑器->XAML->杂项->错误和警告->**取消**显示 XAML 设计器检测到的错误。

### XData ORM

根据模型和 XData 的数据结构互相转换。示例详见[XData.cs](../test/Test/TestCode/XData.cs)。

- `[RegAppORM]`：定义的`RegAppTableRecord`，对应 XData 的第一行。不负责**添加**`RegAppTableRecord`。
- `[XDataORM(0)]`：定义的数据，存放在模型的属性上，不以顺位为基准，以**位置**为基准，`[XDataORM(0)]`对应`ResultBuffer`第二行，`[XDataORM(1)]`可以省略，则`ResultBuffer`第三行为空，`[XDataORM(2)]`对应`ResultBuffer`第四行。顺序从 0 开始，不允许出现重复。
- XData 其对应的`object`值，直接和**类属性**对应。
- 当 XData 为`ExtendedDataAsciiString`，支持像 WPF 一样的类型转换，即 XData 的`string`与模型`属性类型`的互相转换。
- 支出**数组**属性。
- 不支持**泛型**。
