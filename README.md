# AutoCADDotNetLibrary

AutoCADDotNetLibrary：根据实际问题和需求出发，使用 c#对 AutoCAD 二次开发代码进行**简化和封装**。如果在 AutoCAD 二次开发中遇到了问题，此项目或许会找到答案或思路。

![](https://img.shields.io/badge/IDE-vs2022-blue)
![](https://img.shields.io/badge/C%23-7.3-blue.svg)

## 快速开始

由于使用的是[源生成器](https://docs.microsoft.com/zh-cn/dotnet/csharp/roslyn-sdk/source-generators-overview)，因此推荐使用`Visual Studio 2022`**_最新版本_**。使用`用于创建面向.NET 或.NET Standard 的类库的项目`创建项目。`.csproj`大概的写法如下：

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>net461</TargetFramework>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="AutoCADDotNet" Version="0.7.*" />
    <PackageReference Include="AutoCADDotNetLibrary" Version="0.7.*" />
  </ItemGroup>

</Project>
```

## 文档

#### 简化操作

- 使用[源生成器](https://docs.microsoft.com/zh-cn/dotnet/csharp/roslyn-sdk/source-generators-overview)自动生成`[assembly: CommandClass()]`和`[assembly: ExtensionApplication()]`。
- 使用`Attribute`把逻辑转移到具有`CommandMethodAttribute`的方法上，具体执行的**操作逻辑**会得到简化。具体细节请看[文档](/docs/%E7%AE%80%E5%8C%96%E6%93%8D%E4%BD%9C.md)。下面为简化的具体的**操作逻辑**：
  - `IExtensionApplication`
  - 创建`CUI`
  - 创建`快捷菜单`
  - 创建`Com菜单`
  - 创建`Ribbon`
- `XData`引入`ORM`的概念。具体细节请看[文档](/docs/%E7%AE%80%E5%8C%96%E6%93%8D%E4%BD%9C.md#xdata-orm)。
- 面板`Palette`可能会使用的事件，选中事件`SelectEntityEvent`和文档变当前事件`DocumentBecameCurrentEvent`，详见[示例](/test/Test/Test2013/Palette/Palette.cs)。
- `WindowTransformCUI`：`window`的`Ribbon`相关对象变为`cui`的对象。

#### 封装函数

`*Extention.cs`提供了各个类的扩展函数，使程序编写更方便。具体请看源码或示例。

#### 解决问题

1. 版本问题
2. 注册和删除命令`[CommandMethod("")]`
3. `ResultBuffer`简化赋值操作
4. `menuname`系统变量路径小写问题
5. 解决`Document.SendStringToExecute(string, bool, bool, bool)`非同步问题
6. 解决 AutoCAD2006 没有`Editor.SetImpliedSelection(ObjectId[])`函数的问题
7. 解决`Editor.SelectWindowPolygon(Point3dCollection)`框选问题
8. 解决`Curve.GetSplitCurves(DoubleCollection)`的报错问题
9. `AOP`概念
10. `Tolerance`的问题（存在疑问）

详见[文档](/docs/%E8%A7%A3%E5%86%B3%E9%97%AE%E9%A2%98.md)。

#### 算法函数

开源库目前不打算提供算法，理由是有一个算法，就会有第二个，第三个...，此开源库不想变成算法库，如果是个人维护，就更不会写算法函数了。次之想法，提供少量常用算法。目前采用的是第一种想法。

## 测试

### 示例

`Test2010`项目能运行在`AutoCAD2010-2012`的版本，`Test2013`项目能运行在`AutoCAD2013`**及以上**的版本。

### 单元测试

`NunitTest`是自动化的，只要在`AutoCAD2013`中运行`NunitTest.dll`插件，就会自动执行单元测试代码，并打开测试报告。

> 目前**只能**使用`AutoCAD2013`进行单元测试。因为每个 AutoCAD 版本的都存在细微差异，而且单元测试项目没有使用`[CommandMethod("")]`技术，导致在各个版本运行不稳定，会频繁报错，所以单元测试项目无法适应多个版本。

## 不要大而全，只要小而美

我只是一个业余的 AutoCAD 开发者，凭一人之力毕竟有限，所以我不想也不能追求对封装的面面俱到，即不要大而全，即从自己的实际问题和需求出发，不考虑一些我没有使用的 AutoCAD 概念（如约束，规则重定义，视口，视图等）。AutoCADDotNetLibrary 只看重每个函数每个功能是否是“美”的，哪怕只有一个函数一个功能称得上“美”，AutoCADDotNetLibrary 也是成功的，即只要小而美，即对于一个问题，可能有很多种答案，由于无数次重构和思考的原因，每个问题的答案大多数趋近于唯一正确的答案。

## QQ 群

![QQ群](/docs/img/AutoCADDotNetLibrary%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
