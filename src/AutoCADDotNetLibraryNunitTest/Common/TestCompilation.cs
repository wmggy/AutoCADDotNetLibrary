﻿using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Testing;

namespace AutoCADDotNetLibrary.NunitTest
{
    internal static class TestCompilation
    {
        //https://github.com/dotnet/roslyn/blob/main/docs/features/source-generators.cookbook.md#unit-testing-of-generators

        /// <summary>
        /// 生成编译代码的平台
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Compilation CreateCompilationNet40(string source)
        {
            ImmutableArray<MetadataReference> refs = ReferenceAssemblies.NetFramework.Net40.Wpf
                .AddAssemblies(ImmutableArray.Create("System.Deployment", "System.Drawing", "System.Windows.Forms"))
                .AddPackages(ImmutableArray.Create(new PackageIdentity("AutoCADDotNet", "0.7.0")))
                .AddAssemblies(ImmutableArray.Create("Microsoft.CSharp"))
                .ResolveAsync("c#", CancellationToken.None).Result;

            return CSharpCompilation.Create("compilation",
               new[] { CSharpSyntaxTree.ParseText(source) },
               refs.ToArray(),
               new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
        }

        public static Compilation CreateCompilationNet35(string source)
        {
            ImmutableArray<MetadataReference> refs = ReferenceAssemblies.NetFramework.Net35.Wpf
                .AddAssemblies(ImmutableArray.Create("System.Deployment", "System.Drawing", "System.Windows.Forms"))
                .AddPackages(ImmutableArray.Create(new PackageIdentity("AutoCADDotNet", "0.7.0")))
                .AddAssemblies(ImmutableArray.Create("Microsoft.CSharp"))
                .ResolveAsync("c#", CancellationToken.None).Result;

            return CSharpCompilation.Create("compilation",
               new[] { CSharpSyntaxTree.ParseText(source) },
               refs.ToArray(),
               new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
        }
    }
}
