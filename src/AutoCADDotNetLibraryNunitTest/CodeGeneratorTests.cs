﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutoCADDotNetLibrary.NunitTest
{
    [TestClass()]
    public class CodeGeneratorTests
    {
        [TestMethod()]
        public void InitializeTestNet40()
        {
            Compilation inputCompilation = TestCompilation.CreateCompilationNet40("");
            CodeGenerator generator = new CodeGenerator();

            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out Compilation outputCompilation, out System.Collections.Immutable.ImmutableArray<Diagnostic> diagnostics);
            GeneratorDriverRunResult r = driver.GetRunResult();

            List<Diagnostic> r1 = outputCompilation.GetDiagnostics().Where(x => x.DefaultSeverity == DiagnosticSeverity.Error).ToList();
            Debug.Assert(r1.Count == 0, "编译没有发生错误");

            Debug.Assert(r.Results.Any(x => x.GeneratedSources.Any(y => y.HintName.Contains("2013+"))), "存在2013+版本");
        }

        [TestMethod()]
        public void InitializeTestNet35()
        {
            Compilation inputCompilation = TestCompilation.CreateCompilationNet35("");
            CodeGenerator generator = new CodeGenerator();

            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out Compilation outputCompilation, out System.Collections.Immutable.ImmutableArray<Diagnostic> diagnostics);
            GeneratorDriverRunResult r = driver.GetRunResult();

            List<Diagnostic> r1 = outputCompilation.GetDiagnostics().Where(x => x.DefaultSeverity == DiagnosticSeverity.Error).ToList();
            Debug.Assert(r1.Count == 0, "编译没有发生错误");

            Debug.Assert(r.Results.Any(x => x.GeneratedSources.Any(y => y.HintName.Contains("2012-"))), "存在2012-版本");
        }
    }
}