﻿using System.Diagnostics;
using AutoCADDotNetLibrary.Generator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutoCADDotNetLibrary.NunitTest
{
    [TestClass()]
    public class ExtensionApplicationAttributeGeneratorTest
    {
        //已有时，不生成
        [TestMethod()]
        public void ExecuteTest1()
        {
            Compilation inputCompilation = TestCompilation.CreateCompilationNet40(@"

using Autodesk.AutoCAD.Runtime;

[assembly: ExtensionApplication(typeof(Test.Init))]

namespace Test
{
    public class Init : IExtensionApplication
    {
        public void Initialize()
        {
        }

        public void Terminate()
        {
        }
    }
}

");
            ExtensionApplicationAttributeGenerator generator = new ExtensionApplicationAttributeGenerator();

            // Create the driver that will control the generation, passing in our generator
            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out Compilation outputCompilation, out System.Collections.Immutable.ImmutableArray<Diagnostic> diagnostics);
            GeneratorDriverRunResult r = driver.GetRunResult();

            Debug.Assert(r.GeneratedTrees.Length == 0);
        }

        //没有时，生成
        [TestMethod()]
        public void ExecuteTest2()
        {
            Compilation inputCompilation = TestCompilation.CreateCompilationNet40(@"

using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class Init : IExtensionApplication
    {
        public void Initialize()
        {
        }

        public void Terminate()
        {
        }
    }
}

");
            ExtensionApplicationAttributeGenerator generator = new ExtensionApplicationAttributeGenerator();

            // Create the driver that will control the generation, passing in our generator
            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out Compilation outputCompilation, out System.Collections.Immutable.ImmutableArray<Diagnostic> diagnostics);
            GeneratorDriverRunResult r = driver.GetRunResult();

            Debug.Assert(r.GeneratedTrees.Length == 1);
        }
    }
}