﻿using System.Diagnostics;
using AutoCADDotNetLibrary.Generator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutoCADDotNetLibrary.NunitTest
{
    //https://github.com/dotnet/roslyn/blob/main/docs/features/source-generators.cookbook.md#unit-testing-of-generators
    [TestClass()]
    public class CommandClassAttributeGeneratorTest
    {
        [TestMethod()]
        public void ExecuteTest1()
        {
            Compilation inputCompilation = TestCompilation.CreateCompilationNet40(@"

using Autodesk.AutoCAD.Runtime;

[assembly: CommandClass(typeof(namespace1.class1))]
[assembly: CommandClass(typeof(namespace1.class2))]

namespace namespace1
{
    public class class1
    {
        [CommandMethod(""cm1"")]
        public void A()
        {

        }
    }

    public class class2
    {
        [CommandMethod(""cm2"")]
        public void A()
        {

        }
    }

    public class class3
    {
        [CommandMethod(""cm3"")]
        public void A()
        {

        }
    }
}

");
            CommandClassAttributeGenerator generator = new CommandClassAttributeGenerator();

            // Create the driver that will control the generation, passing in our generator
            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out Compilation outputCompilation, out System.Collections.Immutable.ImmutableArray<Diagnostic> diagnostics);
            GeneratorDriverRunResult r = driver.GetRunResult();

            Debug.Assert(r.GeneratedTrees.Length == 1);
        }

        [TestMethod()]
        public void ExecuteTest2()
        {
            Compilation inputCompilation = TestCompilation.CreateCompilationNet40(@"

using Autodesk.AutoCAD.Runtime;

[assembly: CommandClass(typeof(namespace1.class1))]

namespace namespace1
{
    public class class1
    {
        [CommandMethod(""cm1"")]
        public void A()
        {

        }
    }
}

");
            CommandClassAttributeGenerator generator = new CommandClassAttributeGenerator();

            // Create the driver that will control the generation, passing in our generator
            GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

            driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out Compilation outputCompilation, out System.Collections.Immutable.ImmutableArray<Diagnostic> diagnostics);
            GeneratorDriverRunResult r = driver.GetRunResult();

            Debug.Assert(r.GeneratedTrees.Length == 0);
        }
    }
}