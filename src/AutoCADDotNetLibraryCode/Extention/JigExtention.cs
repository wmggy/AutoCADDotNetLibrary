﻿using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 拖拽类扩展
    /// </summary>
    public static class JigExtention
    {
        /// <summary>
        /// 鼠标点的拖拽类
        /// </summary>
        private class Point3dJig : DrawJig
        {
            private Action<Point3d> _act;//定义点的动作
            private List<Entity> _ents;//拖拽实体
            private Point3d _lastPt = new Point3d();//上一个点
            private JigPromptPointOptions _options;//附加信息

            /// <summary>
            /// 定义拖拽类
            /// </summary>
            /// <param name="act">鼠标点的动作函数</param>
            /// <param name="ents">重绘的实体集</param>
            public Point3dJig(Action<Point3d> act, List<Entity> ents)
            {
                _act = act;
                _ents = ents ?? new List<Entity>();
            }

            /// <summary>
            /// 定义拖拽类
            /// </summary>
            /// <param name="message">提示信息</param>
            /// <param name="act">鼠标点的动作函数</param>
            /// <param name="ents">重绘的实体集</param>
            public Point3dJig(string message, Action<Point3d> act, List<Entity> ents) : this(act, ents)
            {
                _options = new JigPromptPointOptions() { Message = "\n" + message.Trim() };
            }

            /// <summary>
            /// 定义拖拽类
            /// </summary>
            /// <param name="options">提示参数</param>
            /// <param name="act">鼠标点的动作函数</param>
            /// <param name="ents">重绘的实体集</param>
            public Point3dJig(JigPromptPointOptions options, Action<Point3d> act, List<Entity> ents) : this(act, ents)
            {
                _options = options;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="draw"></param>
            /// <returns></returns>
            protected override bool WorldDraw(Autodesk.AutoCAD.GraphicsInterface.WorldDraw draw)
            {
                foreach (Entity ent in _ents)
                {
                    draw.Geometry.Draw(ent);
                }
                return true;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="prompts"></param>
            /// <returns></returns>
            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                PromptPointResult resJigPoint = _options == null ? prompts.AcquirePoint() : prompts.AcquirePoint(_options);

                if (resJigPoint.Status == PromptStatus.Cancel)//取消操作
                {
                    return SamplerStatus.Cancel;
                }
                else
                {
                    if (_lastPt.IsEqual(resJigPoint.Value))//不动操作
                        return SamplerStatus.NoChange;

                    _lastPt = resJigPoint.Value;
                    _act?.Invoke(resJigPoint.Value);
                    return SamplerStatus.OK;//继续运行
                }
            }
        }

        /// <summary>
        /// 创建关于鼠标点的Jig
        /// </summary>
        /// <param name="act">鼠标点的动作函数</param>
        /// <param name="ents">重绘的实体集</param>
        /// <returns></returns>
        public static Jig CreatePoint3dJig(Action<Point3d> act, List<Entity> ents)
        {
            return new Point3dJig(act, ents);
        }

        /// <summary>
        /// 创建关于鼠标点的Jig
        /// </summary>
        /// <param name="message">提示信息</param>
        /// <param name="act">鼠标点的动作函数</param>
        /// <param name="ents">重绘的实体集</param>
        /// <returns></returns>
        public static Jig CreatePoint3dJig(string message, Action<Point3d> act, List<Entity> ents)
        {
            return new Point3dJig(message, act, ents);
        }

        /// <summary>
        /// 创建关于鼠标点的Jig
        /// </summary>
        /// <param name="options">提示参数</param>
        /// <param name="act">鼠标点的动作函数</param>
        /// <param name="ents">重绘的实体集</param>
        /// <returns></returns>
        public static Jig CreatePoint3dJig(JigPromptPointOptions options, Action<Point3d> act, List<Entity> ents)
        {
            return new Point3dJig(options, act, ents);
        }
    }
}
