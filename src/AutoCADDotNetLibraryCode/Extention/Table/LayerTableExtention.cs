﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 图层的扩展
    /// </summary>
    public static class LayerTableExtention
    {
        /// <summary>
        /// 设置当前图层
        /// </summary>
        /// <param name="ltr">图层</param>
        public static void SetCurrentLayer(this LayerTableRecord ltr)
        {
            ltr.Database.Clayer = ltr.ObjectId;
        }

        /// <summary>
        /// 操作所有图层（关，冻结，锁）
        /// </summary>
        /// <param name="db">dwg数据库</param>
        /// <param name="isOff">开：false为开,true为关，null不变</param>
        /// <param name="isFrozen">冻结：false为开,true为关，null不变</param>
        /// <param name="isLocked">锁定：false为开,true为关，null不变</param>
        public static void OperatorAllLayerTableRecord(this Database db, bool? isOff, bool? isFrozen, bool? isLocked)
        {
            List<LayerTableRecord> layers = db.GetTableRecords<LayerTableRecord>(OpenMode.ForWrite).ToList();
            layers.ForEach(x =>
            {
                if (isOff.HasValue)
                {
                    x.IsOff = isOff.Value;
                }
                if (isFrozen.HasValue)
                {
                    if (x.ObjectId != db.Clayer)
                    {
                        x.IsFrozen = isFrozen.Value;
                    }
                }
                if (isLocked.HasValue)
                {
                    x.IsLocked = isLocked.Value;
                }
            });
        }
    }
}
