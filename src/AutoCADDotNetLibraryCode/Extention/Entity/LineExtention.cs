﻿using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 线段的扩展
    /// </summary>
    public static class LineExtention
    {
        /// <summary>
        /// 得到中垂线的点
        /// </summary>
        /// <param name="line">线段</param>
        /// <param name="dist">距离</param>
        /// <returns>2个点</returns>
        [Obsolete("bug,此方法会存在输入dist=1，有0.999999的情况")]
        public static Point2d[] GetTPointAtDist(this Line line, double dist)
        {
            //不对
            //LineSegment2d line
            //LineSegment2d l1 = (LineSegment2d)line.GetTrimmedOffset(dist, OffsetCurveExtensionType.Fillet)[0];
            //LineSegment2d l2 = (LineSegment2d)line.GetTrimmedOffset(-dist, OffsetCurveExtensionType.Fillet)[0];
            //return new Point2d[] { l1.MidPoint, l2.MidPoint };

            Point3d pt1 = line.StartPoint;
            Point3d pt2 = line.EndPoint;
            double A = pt2.Y - pt1.Y;
            double B = pt1.X - pt2.X;
            double C = pt1.Y * pt2.X - pt2.Y * pt1.X;

            double C1 = C + dist * Math.Sqrt(A * A + B * B);
            double C2 = C - dist * Math.Sqrt(A * A + B * B);
            double Cchui = A * pt1.Middle(pt2).Y - B * pt1.Middle(pt2).X;

            double d = -(A * A + B * B);
            return new Point2d[] { new Point2d((Cchui * B - C1 * (-A)) / d, (B * C1 - A * Cchui) / d),
                                   new Point2d((Cchui * B - C2 * (-A)) / d, (B * C2 - A * Cchui) / d) };
        }
    }
}
