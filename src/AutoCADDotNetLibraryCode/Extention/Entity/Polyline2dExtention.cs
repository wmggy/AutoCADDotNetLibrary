﻿using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 二维多段线扩展
    /// </summary>
    public static class Polyline2dExtention
    {
        /// <summary>
        /// 二维多段线转换成多段线
        /// </summary>
        /// <param name="poly2d">二维多段线</param>
        /// <returns>多段线</returns>
        public static Polyline ToPolyline(this Polyline2d poly2d)
        {
            Polyline poly = new Polyline();

            Transaction trans = poly2d.Database.TransactionManager.TopTransaction;
            poly2d.Cast<ObjectId>().Select(x => trans.GetObject(x, OpenMode.ForRead)).Cast<Vertex2d>()
                .Where(x => x.VertexType != Vertex2dType.SplineControlVertex).ToList()
                .ForEach(x => poly.AddVertexAt(poly.NumberOfVertices, x.Position.ToPoint2d(), x.Bulge, x.StartWidth, x.EndWidth));

            poly.SetPropertiesFrom(poly2d);
            poly.Elevation = poly2d.Elevation;
            poly.Thickness = poly2d.Thickness;
            poly.Closed = poly2d.Closed;

            return poly;
        }
    }
}
