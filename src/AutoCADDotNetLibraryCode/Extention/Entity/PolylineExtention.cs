﻿using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 多段线扩展
    /// </summary>
    public static class PolylineExtention
    {
        /// <summary>
        /// 获取多段线的所有顶点
        /// </summary>
        /// <param name="poly">多段线</param>
        /// <returns>3维点集</returns>
        public static IEnumerable<Point3d> GetPoint3ds(this Polyline poly)
        {
            for (int i = 0; i < poly.NumberOfVertices; i++)
            {
                yield return poly.GetPoint3dAt(i);
            }
        }

        /// <summary>
        /// 多段线删除重复点
        /// </summary>
        /// <param name="poly">多段线</param>
        public static void RemoveRepeatablePoint(this Polyline poly)
        {
            for (int i = 0; i < poly.NumberOfVertices - 1; i++)
            {
                if (poly.GetPoint2dAt(i).IsEqual(poly.GetPoint2dAt(i + 1)))
                {
                    poly.RemoveVertexAt(i);
                    i--;
                }
            }
            if (poly.Closed && poly.NumberOfVertices > 1)
            {
                if (poly.GetPoint2dAt(0).IsEqual(poly.GetPoint2dAt(poly.NumberOfVertices - 1)))
                {
                    poly.RemoveVertexAt(poly.NumberOfVertices - 1);
                }
            }
        }
    }
}
