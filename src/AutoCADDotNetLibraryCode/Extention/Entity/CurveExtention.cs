﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 曲线扩展
    /// </summary>
    public static class CurveExtention
    {
        /// <summary>
        /// 判断点和多段线的位置关系（-1表示在多段线外部，0表示在多段线上，1表示在多段线内部）
        /// </summary>
        /// <param name="poly">多段线</param>
        /// <param name="pt">点</param>
        /// <returns>-1表示在多段线外部，0表示在多段线上，1表示在多段线内部</returns>
        [Obsolete("这不是最好的方法，只是我的青春记忆")]
        public static int PointOnPolyline(this Polyline poly, Point2d pt)
        {
            List<Point2d> pts = poly.GetPoint3ds().Select(x => x.ToPoint2d()).ToList();

            if (pts.Count == 0)
                return -1;
            if (pts.Count == 1)
            {
                if (pts[0].IsEqual(pt))
                    return 0;
                else
                    return -1;
            }
            if (pts.Count == 2)
            {
                if (pt.IsPtOnPtsBetween(pts[0], pts[1]))
                    return 0;
                else
                    return -1;
            }

            Point3d pt3d = pt.ToPoint3d(poly.Elevation);
            // 1.如果点到多段线的最近点和给定的点重合，表示点在多段线上
            Point3d closestPoint = poly.GetClosestPointTo(pt3d, false);		// 多段线上与给定点距离最近的点
            if (closestPoint.ToPoint2d().IsEqual(pt))			// 点在多段线上
            {
                return 0;
            }
            // 2.第一个射线的方向是从最近点到当前点，起点是当前点
            using (Ray pRay = new Ray())
            {
                pRay.BasePoint = pt3d;

                //找到不与定点相交的点
                Point2d mid = closestPoint.ToPoint2d();
                while (pts.Any(x => (mid - pt).DotProduct(x - pt) == 0))
                {
                    Random rand = new Random();
                    mid = new Point2d(poly.GetPoint2dAt(0).X + rand.Next(0, 10000) / 100,
                        poly.GetPoint2dAt(0).Y + rand.Next(0, 10000) / 100);
                }
                pRay.UnitDir = new Vector3d(-(mid.X - pt.X), -(mid.Y - pt.Y), 0);

                Point3dCollection intPoints = poly.IntersectWith(pRay);

                if (intPoints.Count % 2 == 0)
                    return -1;
                else
                    return 1;
            }
        }

        /// <summary>
        /// 点是否在曲线上
        /// </summary>
        /// <param name="thisCurve">曲线</param>
        /// <param name="pt">点</param>
        /// <returns>是否在曲线上</returns>
        public static bool IsPointOnCurve(this Curve thisCurve, Point3d pt)
        {
            Point3d closestPoint = thisCurve.GetClosestPointTo(pt, false);
            if (closestPoint.IsEqual(pt))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 线与线相交
        /// </summary>
        /// <remarks>CAD2006.net api 存在缺陷,在这里修改bug(存在不在线上的点)</remarks>
        /// <param name="thisCurve">曲线</param>
        /// <param name="curve">曲线</param>
        /// <returns>交点集合</returns>
        public static Point3dCollection IntersectWith(this Curve thisCurve, Curve curve)
        {
            Point3dCollection pts = new Point3dCollection();
#pragma warning disable CS0618
            thisCurve.IntersectWith(curve, Intersect.OnBothOperands, pts, 0, 0);
#pragma warning restore CS0618
            //存在bug,存在多余不在多段线上的点
            for (int i = 0; i < pts.Count; i++)
            {
                if (!thisCurve.IsPointOnCurve(pts[i]))
                {
                    pts.RemoveAt(i);
                    i--;
                }
            }
            for (int i = 0; i < pts.Count; i++)
            {
                if (!curve.IsPointOnCurve(pts[i]))
                {
                    pts.RemoveAt(i);
                    i--;
                }
            }
            return pts;
        }

        /// <summary>
        /// 打断线（当切割点为1，线为圆，样条曲线，椭圆时，不切割）
        /// </summary>
        /// <param name="thisCurve">曲线</param>
        /// <param name="pts">点集</param>
        /// <returns>曲线集</returns>
        public static IEnumerable<Curve> GetSplitCurvesByPoints(this Curve thisCurve, Point3d[] pts)
        {
            if (pts.Length == 0)
                return Enumerable.Empty<Curve>();

            //测试中发现的错误，解决方案是当为1个点时不切割圆，样条曲线，椭圆，详细请看单元测试
            if (pts.Length == 1 && (thisCurve is Circle || thisCurve is Spline || thisCurve is Ellipse))
                return Enumerable.Empty<Curve>();

            double[] ptList = pts.Where(x => thisCurve.IsPointOnCurve(x))
                 .Select(x => thisCurve.GetParameterAtPoint(x))
                 .OrderBy(x => x).ToArray();
            if (ptList.Length == 0)
            {
                return Enumerable.Empty<Curve>();
            }

            return thisCurve.GetSplitCurves(new DoubleCollection(ptList)).Cast<Curve>().ToList();
        }
    }
}