﻿using System.Collections.Generic;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Internal;
using Autodesk.AutoCAD.Runtime;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 常用系统变量
    /// </summary>
    public enum SystemVariableType
    {
        /// <summary>
        /// 控制与读写文件命令一起使用的对话框的显示
        /// </summary>
        FILEDIA,

        /// <summary>
        /// 不显示通常情况下显示的消息（即不进行消息反馈）
        /// </summary>
        NOMUTT,
    }

    /// <summary>
    /// 解决<see cref="Document.SendStringToExecute(string, bool, bool, bool)"/>在函数后才运行，无法使系统变量还原的问题。
    /// 有<see cref="Document.SendStringToExecute(string, bool, bool, bool)"/>时再使用。（不能跨文档运行）
    /// </summary>
    public static class SystemVariable
    {
        private static Dictionary<string, object> _newValue = new Dictionary<string, object>();
        private static Dictionary<string, object> _oldValue = new Dictionary<string, object>();

        /// <summary>
        /// 设置系统变量
        /// </summary>
        /// <param name="systemVariable">系统变量</param>
        /// <param name="value">值</param>
        public static void SetSystemVariable(string systemVariable, object value)
        {
            _newValue.Add(systemVariable, value);//不能检查字典重复，理由：为了防止意外

            object temp = Application.GetSystemVariable(systemVariable);//旧值
            Application.SetSystemVariable(systemVariable, value);
            _oldValue.Add(systemVariable, temp);
        }

        /// <summary>
        /// 设置系统变量
        /// </summary>
        /// <param name="systemVariable">系统变量</param>
        /// <param name="value">值</param>
        public static void SetSystemVariable(SystemVariableType systemVariable, object value)
        {
            SetSystemVariable(systemVariable.ToString(), value);
        }

        /// <summary>
        /// 还原系统变量，放在所有<see cref="Document.SendStringToExecute(string, bool, bool, bool)"/>的最后面
        /// </summary>
        public static void RestoreSystemVariable(Document doc)
        {
            doc.SendStringToExecute("NullCommandSystemVariable ", true, false, false);
            doc.CommandWillStart += RestoreValue;
        }

        //执行空命令，还原值
        private static void RestoreValue(object sender, CommandEventArgs e)
        {
            if ("NullCommandSystemVariable".ToUpper() == e.GlobalCommandName.Trim().ToUpper())//执行空命令，还原值
            {
                foreach (KeyValuePair<string, object> item in _oldValue)
                {
                    Application.SetSystemVariable(item.Key, item.Value);
                }
                _newValue.Clear();
                _oldValue.Clear();

                ((Document)sender).CommandWillStart -= RestoreValue;
            }
        }

        static SystemVariable()
        {
            Utils.AddCommand(nameof(AutoCADDotNetLibrary), "___NullCommandSystemVariable", "___NullCommandSystemVariable", CommandFlags.NoHistory, () => { });
        }

        /// <summary>
        /// 空命令
        /// </summary>
        //[CommandMethod("___NullCommandSystemVariable", CommandFlags.NoHistory)]
        //public static void NullCommandSystemVariable()
        //{
        //}
    }
}
