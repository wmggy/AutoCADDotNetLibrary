﻿using System;
using System.Runtime.InteropServices;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;

namespace AutoCADDotNetLibrary
{
    internal static class CADVersion
    {
        public static string RegistryProductRootKey(this HostApplicationServices app)
        {
            return app.RegistryProductRootKey;
        }
    }

    /// <summary>
    /// objectarx,c++的函数
    /// </summary>
    public static class ARX
    {
        //AutoCAD2012版本以下
        [DllImport("acad.exe", EntryPoint = "acedCmd", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        private static extern int acedCmd(IntPtr rbp);

        /// <summary>
        /// 同步执行命令
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static int AcedCmd(ResultBuffer args)
        {
            //由于acedCmd只能在程序环境下运行，因此需调用此语句
            if (!Application.DocumentManager.IsApplicationContext)
                return acedCmd(args.UnmanagedObject);
            else
                return 0;
        }
    }
}
