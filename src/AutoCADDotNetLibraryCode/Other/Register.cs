﻿using System.IO;
using System.Linq;
using System.Reflection;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Microsoft.Win32;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 注册CAD(无版本限制)
    /// </summary>
    public static class Register
    {
        /// <summary>
        /// 受信任目录
        /// </summary>
        /// <param name="assembly">程序集</param>
        public static void TrustePaths(Assembly assembly)
        {
            //2013及以下不用这个
            if (Application.Version < new System.Version(19, 1, 0))
                return;

            string value = (string)Application.GetSystemVariable("TRUSTEDPATHS");

            if (value.Split(';').Any(x => x == Path.GetDirectoryName(assembly.Location)))
                return;

            if (!string.IsNullOrEmpty(value))
                value += ";";

            value += Path.GetDirectoryName(assembly.Location);
            Application.SetSystemVariable("TRUSTEDPATHS", value);
        }

        /// <summary>
        /// 注册dll
        /// </summary>
        /// <param name="dllfile">dll文件路径</param>
        /// <returns></returns>
        public static bool RegisteringCAD(string dllfile)
        {
            RegistryKey user = Registry.CurrentUser.OpenSubKey(HostApplicationServices.Current.RegistryProductRootKey() + "\\Applications", true);
            if (user == null)
            {
                return false;
            }

            RegistryKey keyUserApp = user.CreateSubKey(Path.GetFileNameWithoutExtension(dllfile));
            keyUserApp.SetValue("DESCRIPTION", Path.GetFileNameWithoutExtension(dllfile), RegistryValueKind.String);
            keyUserApp.SetValue("LOADCTRLS", 2, RegistryValueKind.DWord);
            keyUserApp.SetValue("LOADER", dllfile, RegistryValueKind.String);
            keyUserApp.SetValue("MANAGED", 1, RegistryValueKind.DWord);

            return true;
        }

        /// <summary>
        /// 注册调用此函数的dll
        /// </summary>
        /// <param name="assembly">程序集</param>
        /// <returns></returns>
        public static bool RegisteringCAD(Assembly assembly)
        {
            return RegisteringCAD(assembly.Location);
        }

        /// <summary>
        /// 删除注册的dll
        /// </summary>
        /// <param name="file">dll文件路径</param>
        /// <returns></returns>
        public static bool DeleteRegisteredCAD(string file)
        {
            RegistryKey user = Registry.CurrentUser.OpenSubKey(HostApplicationServices.Current.RegistryProductRootKey() + "\\Applications", true);
            if (user == null)
            {
                return false;
            }

            if (user.GetSubKeyNames().Contains(Path.GetFileNameWithoutExtension(file)))
            {
                user.DeleteSubKey(Path.GetFileNameWithoutExtension(file));
                return true;
            }

            return false;
        }

        /// <summary>
        /// 删除调用此函数的dll的注册
        /// </summary>
        /// <param name="assembly">程序集</param>
        /// <returns></returns>
        public static bool DeleteRegisteredCAD(Assembly assembly)
        {
            return DeleteRegisteredCAD(assembly.Location);
        }
    }
}