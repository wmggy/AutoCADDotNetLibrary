﻿using System;
using Autodesk.AutoCAD.DatabaseServices;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// XData转换
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class XDataORMAttribute : Attribute
    {
        /// <summary>
        /// XData转换
        /// </summary>
        /// <param name="index">XData的位置顺序</param>
        public XDataORMAttribute(int index)
        {
            Index = index;
        }
        /// <summary>
        /// XData的位置顺序，从0开始，不允许出现重复
        /// </summary>
        public int Index { get; }

        /// <summary>
        /// 当<see cref="DxfCode.ExtendedDataAsciiString"/>时，支持<see cref="System.ComponentModel.TypeConverterAttribute"/>转换器
        /// </summary>
        public DxfCode DxfCode { get; set; } = DxfCode.ExtendedDataAsciiString;
    }
}
