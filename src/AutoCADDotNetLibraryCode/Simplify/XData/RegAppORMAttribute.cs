﻿using System;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// XData转换成模型（不负责添加RegAppTableRecord）
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class RegAppORMAttribute : Attribute
    {
        /// <summary>
        /// XData转换成模型
        /// </summary>
        /// <param name="regAppTableName">RegApp</param>
        public RegAppORMAttribute(string regAppTableName)
        {
            RegAppTableName = regAppTableName;
        }

        /// <summary>
        /// RagApp名字
        /// </summary>
        public string RegAppTableName { get; }
    }
}
