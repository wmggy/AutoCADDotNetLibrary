﻿using System;
using System.Windows.Input;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.Windows;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// wpf命令
    /// </summary>
    public class RibbonCommand : ICommand
    {
        private RibbonCommand()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
#pragma warning disable 67
        public event EventHandler CanExecuteChanged;
#pragma warning restore 67

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            RibbonButton button = parameter as RibbonButton;
            if (button == null || button.CommandParameter == null)
                return;

            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc?.SendStringToExecute((char)27 + button.CommandParameter.ToString().Trim() + "\n", true, false, false);
        }

        /// <summary>
        /// 实例
        /// </summary>
        public static RibbonCommand Instance { get; } = new RibbonCommand();
    }
}
