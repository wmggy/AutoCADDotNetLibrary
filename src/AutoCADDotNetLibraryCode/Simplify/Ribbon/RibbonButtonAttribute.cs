﻿using System;
using Autodesk.AutoCAD.Customization;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 功能区按钮，代表命令
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class RibbonButtonAttribute : Attribute
    {
        /// <summary>
        /// 功能区按钮，代表命令
        /// </summary>
        /// <param name="text">显示文本</param>
        /// <param name="index">顺序</param>
        public RibbonButtonAttribute(string text, int index)
        {
            Text = text;
            Index = index;
        }

        /// <summary>
        /// 显示文本
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// 顺序
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// 按钮样式
        /// </summary>
        public RibbonButtonStyle RibbonButtonStyle { get; set; }

        /// <summary>
        /// 元素Id（AutoCAD会自动生成）
        /// </summary>
        public string UId { get; set; }

        /// <summary>
        /// 小图像(Uri得到图片)
        /// </summary>
        public string SmallImage { get; set; }

        /// <summary>
        /// 大图像(Uri得到图片)
        /// </summary>
        public string LargeImage { get; set; }

        /// <summary>
        /// 是否使用提示<see cref="AutoCADDotNetLibrary.RibbonToolTipManage"/>
        /// </summary>
        public bool IsUseRibbonToolTipManage { get; set; } = true;
    }
}
