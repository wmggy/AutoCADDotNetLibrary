﻿using System.Text.RegularExpressions;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 通过<see cref="AutoCADDotNetLibrary.RibbonButtonAtt"/>设置，管理<see cref="Autodesk.Windows.RibbonToolTip"/>。
    /// </summary>
    public class RibbonToolTipManage
    {
        /// <summary>
        /// 创建RibbonToolTip
        /// </summary>
        /// <param name="commandMethod">CommandMethodAttribute</param>
        /// <param name="ribbonButton">RibbonButtonAttribute</param>
        /// <returns>RibbonToolTip</returns>
        public virtual RibbonToolTip CreateRibbonToolTip(CommandMethodAttribute commandMethod, RibbonButtonAttribute ribbonButton)
        {
            RibbonToolTip toolTip = new RibbonToolTip();
            toolTip.Title = Regex.Replace(ribbonButton.Text, "\\s", "");
            toolTip.Command = commandMethod.GlobalName;

            return toolTip;
        }

        /// <summary>
        /// 通过此变量设置创建
        /// </summary>
        public static RibbonToolTipManage Instance { get; set; } = new RibbonToolTipManage();
    }
}
