﻿using System;
using Autodesk.AutoCAD.ApplicationServices;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 当CAD应用程序完成处理并即将进入空闲状态时发生<see cref="Autodesk.AutoCAD.ApplicationServices.Application.Idle"/>
    /// </summary>
    public static class ApplicationIdle
    {
        private static Action _act;
        /// <summary>
        /// 空闲时间执行的一次性方法（允许嵌套，请不要太复杂，越复杂越出错，最好只是执行一次）
        /// </summary>
        /// <param name="act"></param>
        public static void IdleMethod(Action act)
        {
            _act = act;
            Application.Idle += Application_Idle;
        }

        private static void Application_Idle(object sender, EventArgs e)
        {
            try
            {
                _act?.Invoke();
            }
            finally
            {
                Application.Idle -= Application_Idle;
            }
        }
    }
}
