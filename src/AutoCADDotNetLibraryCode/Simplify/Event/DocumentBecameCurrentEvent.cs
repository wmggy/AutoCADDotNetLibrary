﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Windows;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// dwg变为当前文档事件
    /// </summary>
    public class DocumentBecameCurrentEvent
    {
        private class ConnectEvent
        {
            public ConnectEvent(IReadEntityEvent iReadEntityEvent)
            {
                IReadEntityEvent = iReadEntityEvent;
            }

            public bool IsConnect { get; set; } = true;

            public IReadEntityEvent IReadEntityEvent { get; set; }
        }

        private static void DocumentCollectionEventHandler(object sender, DocumentCollectionEventArgs e)
        {
            if (e.Document == null)
                return;

            List<KeyValuePair<string, ConnectEvent>> temp = _keys
                .Where(x => x.Value.IsConnect && x.Value.IReadEntityEvent != null && x.Value.IReadEntityEvent.IsUseEvent).ToList();
            if (temp.Count == 0)
                return;

            using (Transaction trans = e.Document.TransactionManager.StartTransaction())
            {
                Entity[] ents = e.Document.Database.GetTableRecord<BlockTableRecord>(BlockTableRecord.ModelSpace, OpenMode.ForRead).Cast<ObjectId>()
                                                   .Select(x => x.GetObject(OpenMode.ForRead, false, true)).Cast<Entity>().ToArray();

                foreach (KeyValuePair<string, ConnectEvent> item in temp)
                {
                    item.Value.IReadEntityEvent.ReadEntities(e.Document, ents);
                }
            }

        }

        private static Dictionary<string, ConnectEvent> _keys = new Dictionary<string, ConnectEvent>();
        private static bool IsAddEvent = false;

        /// <summary>
        /// 添加事件
        /// </summary>
        /// <param name="eventName">事件名称</param>
        /// <param name="iSelectEntityEvent">读取实体接口</param>
        public static void Add(string eventName, IReadEntityEvent iSelectEntityEvent)
        {
            _keys.Add(eventName, new ConnectEvent(iSelectEntityEvent));

            if (!IsAddEvent)
            {
                Application.DocumentManager.DocumentBecameCurrent += DocumentCollectionEventHandler;
                IsAddEvent = true;
            }
        }

        /// <summary>
        /// 添加PaletteSet的StateChange显示
        /// </summary>
        /// <param name="ps">面板</param>
        /// <param name="eventName">事件名称</param>
        public static void SetStateChanged(PaletteSet ps, string eventName)
        {
            ps.StateChanged += (sender, e) =>
            {
                if (e.NewState == StateEventIndex.Show)
                {
                    Add(eventName);
                }
                else if (e.NewState == StateEventIndex.Hide)
                {
                    Remove(eventName);
                }
            };
        }

        /// <summary>
        /// 添加事件
        /// </summary>
        /// <param name="eventName">事件名称</param>
        public static void Add(string eventName)
        {
            if (_keys.ContainsKey(eventName))
            {
                _keys[eventName].IsConnect = true;
            }
        }

        /// <summary>
        /// 移除事件
        /// </summary>
        /// <param name="eventName">事件名称</param>
        public static void Remove(string eventName)
        {
            if (_keys.ContainsKey(eventName))
            {
                _keys[eventName].IsConnect = false;
            }
        }
    }
}
