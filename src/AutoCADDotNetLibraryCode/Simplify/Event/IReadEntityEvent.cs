﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 读取实体接口
    /// </summary>
    public interface IReadEntityEvent
    {
        /// <summary>
        /// 读取实体
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="ents"></param>
        void ReadEntities(Document doc, Entity[] ents);

        /// <summary>
        /// 是否使用事件
        /// </summary>
        bool IsUseEvent { get; set; }
    }
}
