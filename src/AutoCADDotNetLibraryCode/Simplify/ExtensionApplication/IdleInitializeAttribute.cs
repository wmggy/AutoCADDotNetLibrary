﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 第一次空闲时间<see cref="Autodesk.AutoCAD.ApplicationServices.Application.Idle"/>执行初始化（要求：公有无参方法，如果是实例，则要有无参构造函数）
    /// 目的是解决初始化时无法使用AuotCad命令的问题。bug:里面的命令耗时时，容易造成初始化界面卡顿。
    /// </summary>
    public class IdleInitializeAttribute : InitializeAttribute
    {
        /// <summary>
        /// 延迟执行初始化的函数
        /// </summary>
        private static List<Action> _acts = new List<Action>();
        /// <summary>
        /// 调用方法
        /// </summary>
        /// <param name="method">需要执行Idle初始化的函数</param>
        internal override void InvokeMethod(MethodInfo method)
        {
            if (_acts.Count == 0)//连接1次
            {
                ApplicationIdle.IdleMethod(() =>
                {
                    _acts.ForEach(x => x?.Invoke());//空闲时间执行初始化的函数
                });
            }
            _acts.Add(() => base.InvokeMethod(method));//延迟执行
        }
    }
}
