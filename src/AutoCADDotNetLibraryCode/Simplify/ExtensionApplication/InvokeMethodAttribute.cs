﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace AutoCADDotNetLibrary
{
    /// <summary>
    /// 调用使用这个Attribute的方法（要求：公有无参方法，如果是实例，则要有无参构造函数）
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public abstract class InvokeMethodAttribute : Attribute
    {
        /// <summary>
        /// 顺序，越小越先执行，越大越后执行，默认为0
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// 如果有true，测试这个特性，其他为false的全部关闭
        /// </summary>
        public bool IsDebug { get; set; } = false;

        /// <summary>
        /// 重复利用类实例
        /// </summary>
        private static Dictionary<Type, object> _objs = new Dictionary<Type, object>();
        /// <summary>
        /// 执行使用特性的方法
        /// </summary>
        /// <param name="method">使用这个特性的方法</param>
        internal virtual void InvokeMethod(MethodInfo method)
        {
            if (method.IsStatic)
            {
                method.Invoke(null, null);
            }
            else
            {
                if (!_objs.ContainsKey(method.ReflectedType))//重复利用类实例
                {
                    _objs.Add(method.ReflectedType, method.ReflectedType.GetConstructor(new Type[] { }).Invoke(null));//空参数的构造函数
                }
                method.Invoke(_objs[method.ReflectedType], null);
            }
        }
    }

    /// <summary>
    /// CAD初始化特性（要求：公有无参方法，如果是实例，则要有无参构造函数）
    /// </summary>
    public class InitializeAttribute : InvokeMethodAttribute
    {
    }
    /// <summary>
    /// CAD终止化特性（要求：公有无参方法，如果是实例，则要有无参构造函数）
    /// </summary>
    public class TerminateAttribute : InvokeMethodAttribute
    {
    }
}
