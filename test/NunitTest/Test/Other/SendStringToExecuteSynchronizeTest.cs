﻿using System.Collections.Generic;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "3.2.Other.SendStringToExecuteSynchronize")]
    public class SendStringToExecuteSynchronizeTest
    {
        public static Dictionary<string, bool> _assert = new Dictionary<string, bool>()
        {
            { "第一次同步执行",false },
            { "第二次同步执行",false },
        };

        [IdleInitialize]
        public void RunCADOtherTests()
        {
            Application.DocumentManager.MdiActiveDocument.SendStringToExecute("filedia 0\n", true, false, false);
            Application.DocumentManager.MdiActiveDocument.SendStringToExecuteSynchronizeMehtod(() =>
            {
                int f = int.Parse(Application.GetSystemVariable("filedia").ToString());
                if (f == 0)
                {
                    _assert["第一次同步执行"] = true;
                }
            });
            Application.DocumentManager.MdiActiveDocument.SendStringToExecute("filedia 1\n", true, false, false);
            Application.DocumentManager.MdiActiveDocument.SendStringToExecuteSynchronizeMehtod(() =>
            {
                int f = int.Parse(Application.GetSystemVariable("filedia").ToString());
                if (f == 1)
                {
                    _assert["第二次同步执行"] = true;
                }
            });
        }

        [Test(Description = "测试SendStringToExecuteSynchronize")]
        public void NunitTestInitialize()
        {
            foreach (KeyValuePair<string, bool> item in _assert)
            {
                Assert.IsTrue(item.Value, item.Key);
            }
        }
    }
}
