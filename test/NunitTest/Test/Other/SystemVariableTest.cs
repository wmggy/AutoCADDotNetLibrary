﻿using System.Collections.Generic;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "3.3.Other.SystemVariable")]
    public class SystemVariableTest
    {
        public static Dictionary<string, bool> _assert = new Dictionary<string, bool>()
        {
            { "系统变量还原",false },
        };

        [IdleInitialize]
        public void RunCADOtherTests()
        {
            Application.SetSystemVariable("FILEDIA", 1);
            SystemVariable.SetSystemVariable(SystemVariableType.FILEDIA, 0);

            SystemVariable.RestoreSystemVariable(Application.DocumentManager.MdiActiveDocument);

            Application.DocumentManager.MdiActiveDocument.SendStringToExecuteSynchronizeMehtod(() =>
            {
                int f = int.Parse(Application.GetSystemVariable("filedia").ToString());
                if (f == 1)
                {
                    _assert["系统变量还原"] = true;
                }
            });
        }

        [Test(Description = "测试SystemVariableTest")]
        public void NunitTestInitialize()
        {
            foreach (KeyValuePair<string, bool> item in _assert)
            {
                Assert.IsTrue(item.Value, item.Key);
            }
        }
    }
}
