﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "4.6.Simplify.XData")]
    public class XDataTest
    {
        [Test(Description = "测试ORM")]
        public void NunitXData()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = Application.DocumentManager.GetDocument(db);

            using (doc.LockDocument())
            {
                using (Transaction trans = doc.TransactionManager.StartTransaction())
                {
                    doc.Database.AddTableRecord<RegAppTableRecord>("Model");

                    Line ent = new Line(new Point3d(0, 0, 0), new Point3d(10, 10, 0));

                    //集合初始值设定项中的扩展 Add 方法
                    List<TypedValue> list1 = new List<TypedValue>()
                    {
                        { DxfCode.ExtendedDataAsciiString,"1" },
                        { DxfCode.ExtendedDataAsciiString,"2" },
                    };
                    List<TypedValue> list2 = new List<TypedValue>()
                    {
                        { list1 },
                        { DxfCode.ExtendedDataAsciiString,"3" },
                    };
                    ResultBuffer list3 = new ResultBuffer()
                    {
                        { list2,null },
                        { DxfCode.ExtendedDataAsciiString,"4" },
                    };
                    Assert.True(list1.Count == 2 && list2.Count == 3 && list3.AsArray().Length == 4, "Add扩展方法测试");


                    //null测试
                    ent.SetXdataByModel<Model>(null);
                    Assert.IsNull(ent.GetModelByXdata<Model>(), "null值测试");

                    //错误测试
                    Assert.Catch(typeof(Exception), () =>
                    {
                        ent.XData = new ResultBuffer()
                        {
                            { DxfCode.ExtendedDataRegAppName,"Model" },
                            { DxfCode.ExtendedDataAsciiString,"" },//0
                            { DxfCode.ExtendedDataAsciiString,"" },//1
                            { DxfCode.ExtendedDataAsciiString,"1" },//2
                            { DxfCode.ExtendedDataInteger32,0 },//3
                            { DxfCode.ExtendedDataAsciiString,"pt(1,1)" },//4
                            { DxfCode.ExtendedDataAsciiString,"" },//5
                        };

                        Model error = ent.GetModelByXdata<Model>(true);
                    }, "转换错误测试，抛出错误");

                    Assert.DoesNotThrow(() =>
                    {
                        ent.XData = new ResultBuffer()
                        {
                            { DxfCode.ExtendedDataRegAppName,"Model" },
                            { DxfCode.ExtendedDataAsciiString,"" },//0
                            { DxfCode.ExtendedDataAsciiString,"" },//1
                            { DxfCode.ExtendedDataAsciiString,"1" },//2
                            { DxfCode.ExtendedDataInteger32,0 },//3
                            { DxfCode.ExtendedDataAsciiString,"pt(1,1)" },//4
                            { DxfCode.ExtendedDataAsciiString,"" },//5
                        };

                        Model error = ent.GetModelByXdata<Model>();
                    }, "转换错误测试，不抛出错误");


                    //值类型缺失测试，XDataORM(2)无值，可以转换成默认值0，没报错。
                    Assert.DoesNotThrow(() =>
                    {
                        ent.XData = new ResultBuffer()
                        {
                            { DxfCode.ExtendedDataRegAppName,"Model" },
                            { DxfCode.ExtendedDataAsciiString,"1" },//0
                        };

                        Model valueModel = ent.GetModelByXdata<Model>();
                    }, "缺少数据转化，值类型可以为default值测试");


                    //model测试
                    Model setModel = new Model()
                    {
                        p1 = "p1",
                        p2 = 222,
                        p3 = 333,
                        p4 = new string[] { "1", "2", "3" },
                        p5 = new List<int>() { 1, 2, 3 },
                        p6 = new Point2d(0.2, 0.3),
                        p7 = new System.Windows.Thickness(11, 22, 33, 44)
                    };

                    ent.SetXdataByModel<Model>(setModel);

                    Model getModel = ent.GetModelByXdata<Model>();

                    Assert.True(getModel.p1 == setModel.p1
                        && getModel.p2 == setModel.p2
                        && getModel.p3 == setModel.p3
                        && getModel.p4.SequenceEqual(setModel.p4)
                        && getModel.p5.SequenceEqual(setModel.p5)
                        && getModel.p6 == setModel.p6
                        && getModel.p7 == setModel.p7, "模型数据转换测试");
                }
            }
        }


        [RegAppORM("Model")]
        public class Model
        {
            [XDataORM(0)]
            public string p1 { get; set; } = "";

            //允许跳过1，但最好不要

            [XDataORM(2)]
            public int p2 { get; set; }

            [XDataORM(3, DxfCode = DxfCode.ExtendedDataInteger32)]
            public int p3 { get; set; }

            //数组，与项对应
            [XDataORM(4)]
            public string[] p4 { get; set; }

            //数组，与项对应
            [XDataORM(5, DxfCode = DxfCode.ExtendedDataInteger32)]
            public List<int> p5 { get; set; }

            //自定义类型装换器
            [XDataORM(6)]
            [TypeConverter(typeof(Point2dConverter))]
            public Point2d p6 { get; set; }

            //类上的转换器[TypeConverter(typeof(ThicknessConverter))]
            [XDataORM(7)]
            public System.Windows.Thickness p7 { get; set; }

            //测试：增加模型的属性，不影响其他代码
            [XDataORM(8)]
            public string p8 { get; set; }
        }

        public class Point2dConverter : TypeConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return sourceType == typeof(string);
            }
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
            {
                return destinationType == typeof(Point2d);
            }
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
            {
                return $"Point2d({((Point2d)value).X},{((Point2d)value).Y})";
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                Match pt = Regex.Match((string)value, @"^Point2d\(([0-9|\.]*),([0-9|\.]*)\)$");
                if (pt.Success)
                {
                    return new Point2d(double.Parse(pt.Groups[1].Value), double.Parse(pt.Groups[2].Value));
                }
                else
                {
                    throw new System.Exception("点格式错误");
                }
            }
        }
    }
}
