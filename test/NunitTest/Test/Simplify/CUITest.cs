﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Customization;
using Autodesk.AutoCAD.Runtime;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "4.3.Simplify.CUI")]
    public class CUITest
    {
        public static readonly string CUIXFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\NunitTest.cuix";

        [Initialize(Index = -100)]//先执行
        public static void CreateCUI()
        {
            CustomizationSection cs = new CustomizationSection();
            cs.MenuGroupName = "NunitTest";

            //宏
            MacroGroup mg = new MacroGroup("NunitTestGroup", cs.MenuGroup);
            Dictionary<string, MenuMacro> macros = mg.AddAssemblyMenuMacro(Assembly.GetExecutingAssembly());

            //下拉菜单
            PopMenu pm = new PopMenu("NunitTestMenu", new System.Collections.Specialized.StringCollection() { "别名1", }, "", cs.MenuGroup);//别名不能为空
            pm.ElementID = "NunitPopMenuId";
            pm.AddPopMenuItem(macros, typeof(CUITest));
            pm.AddPopMenu("二级菜单", macros, typeof(CUITest))
              .AddPopMenu("三级菜单", macros, typeof(CUITest))
              .AddPopMenu("四级菜单", macros, typeof(CUITest));

            cs.SaveAs(CUIXFilePath);

            //重新加载
            Application.UnloadPartialMenu(CUIXFilePath);
            Application.LoadPartialMenu(CUIXFilePath);
            Application.ReloadAllMenus();
        }

        [PopMenuItem(1, Name = "NunitTestPopMenuItem", UID = "pi1"), MenuMacro("直线", UID = "l1", CLICommand = "cli_l1", HelpString = "help_l1", SmallImageFileName = "CUIImage/B.ico", LargeImageFileName = "CUIImage/A.ico")]
        [CommandMethod("NunitCUI1")]
        public void NunitCUI1()
        {
        }

        [PopMenuItem(2, UID = "pi2"), MenuMacro("多段线", UID = "l2", CLICommand = "cli_l2", HelpString = "help_l2", SmallImageFileName = "CUIImage/B.ico", LargeImageFileName = "CUIImage/A.ico")]
        [CommandMethod("NunitCUI2")]
        public void NunitCUI2()
        {
        }

        [Test(Description = "测试CUI")]
        public void NunitCUI()
        {
            CustomizationSection cs = new CustomizationSection(CUITest.CUIXFilePath);


            MenuMacro menu1 = cs.getMenuMacro("l1");
            MenuMacro menu2 = cs.getMenuMacro("l2");

            Assert.IsTrue(menu1.macro.CLICommand == "cli_l1" && menu1.macro.HelpString == "help_l1" && menu1.macro.Command.Contains("NunitCUI1")
                && menu1.macro.LargeImageBitmap != null && menu1.macro.SmallImageBitmap != null && menu1.macro.Name == "直线", "菜单项1获得成功");
            Assert.IsTrue(menu2.macro.CLICommand == "cli_l2" && menu2.macro.HelpString == "help_l2" && menu2.macro.Command.Contains("NunitCUI2")
               && menu2.macro.LargeImageBitmap != null && menu2.macro.SmallImageBitmap != null && menu2.macro.Name == "多段线", "菜单项2获得成功");


            PopMenu pm = cs.getPopMenu("NunitPopMenuId");

            Assert.IsTrue(pm.PopMenuItems[0].ElementID == "pi1" && ((PopMenuItem)pm.PopMenuItems[0]).Name == "NunitTestPopMenuItem", "PopMenuItem1获得成功");
            Assert.IsTrue(pm.PopMenuItems[1].ElementID == "pi2" && ((PopMenuItem)pm.PopMenuItems[1]).Name == "多段线", "PopMenuItem2获得成功");
        }


        [Test(Description = "测试AcadPopupMenuItemAtt")]
        public void NunitAcadPopupMenuItemAtt()
        {
            dynamic acadApp = (dynamic)Application.AcadApplication;

            dynamic r1 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "NUNITTEST", "NunitTestMenu", "NunitTestPopMenuItem");
            dynamic r2 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "NUNITTEST", "NunitTestMenu", "多段线");

            Assert.IsTrue(r1.Label == "NunitTestPopMenuItem" && r2.Label == "多段线", "动态菜单项值获得成功");
            Assert.IsTrue(r1.Macro.Contains("NunitCUI1") && r2.Macro.Contains("NunitCUI2"), "Macro");
            Assert.IsTrue(r1.TagString == "l1" && r2.TagString == "l2", "TagString");
            Assert.IsTrue(r1.HelpString == "help_l1" && r2.HelpString == "help_l2", "HelpString");


            dynamic r3 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "NUNITTEST", "NunitTestMenu", "二级菜单", "NunitTestPopMenuItem");
            dynamic r4 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "NUNITTEST", "NunitTestMenu", "二级菜单", "三级菜单", "NunitTestPopMenuItem");
            dynamic r5 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "NUNITTEST", "NunitTestMenu", "二级菜单", "三级菜单", "四级菜单", "NunitTestPopMenuItem");

            Assert.IsTrue(r3 != null && r4 != null && r5 != null, "DynamicMenuItemAtt测试");
        }

    }
}
