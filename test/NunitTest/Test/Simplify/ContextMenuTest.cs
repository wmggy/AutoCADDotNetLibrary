﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "4.2.Simplify.ContextMenu")]
    public class ContextMenuTest
    {
        [ContextMenuItem("快捷菜单直线", 1)]
        [CommandMethod("NunitContextMenu1")]
        public void NunitContextMenu1()
        {
        }

        [ContextMenuItem("快捷菜单多段线", 2, IconName = "NunitTest.ContextMenuImage.曲线.ico", Checked = true, Enabled = false, Visible = false)]
        [CommandMethod("NunitContextMenu2")]
        public void NunitContextMenu2()
        {
        }


        [Test(Description = "测试ContextMenu")]
        public void NunitContextMenu()
        {
            ContextMenuExtension cme = new ContextMenuExtension();
            MenuItem it = new MenuItem("快捷菜单实体");
            MenuItem[] menuItems = it.AddContextMenuItem(typeof(ContextMenuTest));
            cme.MenuItems.Add(it);

            RXClass rxc = Entity.GetClass(typeof(Entity));
            Application.AddObjectContextMenuExtension(rxc, cme);


            //测试
            Assert.IsTrue(menuItems[0].Checked == false &&
                menuItems[0].Enabled == true &&
                menuItems[0].Icon == null &&
                menuItems[0].Text == "快捷菜单直线" &&
                menuItems[0].Visible == true, "ContextMenu项1测试");

            Assert.IsTrue(menuItems[1].Checked == true &&
                menuItems[1].Enabled == false &&
                menuItems[1].Icon != null &&
                menuItems[1].Text == "快捷菜单多段线" &&
                menuItems[1].Visible == false, "ContextMenu项2测试");
        }
    }
}
