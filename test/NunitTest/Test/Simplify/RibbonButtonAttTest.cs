﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.Customization;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using NUnit.Framework;
using RibbonButton = Autodesk.Windows.RibbonButton;

namespace NunitTest
{
    [TestFixture(TestName = "4.5.Simplify.RibbonButton")]
    public class RibbonButtonAttTest
    {
        [RibbonButton("直线", 3, RibbonButtonStyle = RibbonButtonStyle.LargeWithHorizontalText,
            SmallImage = "RibbonImage/B.ico", LargeImage = "RibbonImage/A.ico", IsUseRibbonToolTipManage = false)]
        [CommandMethod("NunitTestRibbonButtonAtt1")]
        public void NunitTestRibbonButtonAtt1()
        {
        }

        [RibbonButton("直线", 4, RibbonButtonStyle = RibbonButtonStyle.SmallWithoutText,
            SmallImage = "pack://application:,,,/NunitTest;component/RibbonImage/B.ico", LargeImage = "RibbonImage/A.ico")]
        [CommandMethod("NunitTestRibbonButtonAtt2")]
        public void NunitTestRibbonButtonAtt2()
        {
        }



        [Test(Description = "测试SetRibbonButtonStyle")]
        public static void NunitTestRibbonButtonAtt()
        {
            RibbonButton rb = new RibbonButton();
            RibbonButtonAtt.SetType(rb, typeof(RibbonButtonAttTest));
            RibbonButtonAtt.SetIndex(rb, 3);

            Assert.IsTrue(rb.ShowText == true && rb.Size == RibbonItemSize.Large && rb.Orientation == System.Windows.Controls.Orientation.Horizontal
                && rb.LargeImage != null && rb.Image != null && rb.ToolTip == null, "测试RibbonButtonAtt");



            RibbonButtonAtt.SetIndex(rb, 4);
            RibbonButtonAtt.SetType(rb, typeof(RibbonButtonAttTest));

            Assert.IsTrue(rb.ShowText == false && rb.Size == RibbonItemSize.Standard && rb.Orientation == System.Windows.Controls.Orientation.Vertical
                 && rb.LargeImage != null && rb.Image != null && rb.ToolTip != null, "测试RibbonButtonAtt赋值顺序");
        }


    }
}
