﻿using System.Collections.Generic;
using AutoCADDotNetLibrary;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "4.4.Simplify.ExtensionApplication")]
    public class ExtensionApplicationTest
    {
        public static List<int> _index = new List<int>();

        [Initialize(Index = -1)]
        public void M1()
        {
            _index.Add(-1);
        }

        [Initialize]
        public void M2()
        {
            _index.Add(0);
        }

        [Initialize(Index = 1)]
        public void M3()
        {
            _index.Add(1);
        }



        [Test(Description = "测试Initialize执行顺序")]
        public void NunitTest()
        {
            Assert.IsTrue(_index.Count == 3 && _index[0] == -1 && _index[1] == 0 && _index[2] == 1, "测试Initialize执行顺序");
        }

        [Test(Description = "测试Initialize参数Assembly")]
        public void NunitTestAss()
        {
            Assert.IsTrue(object.Equals(Init.GetTestValue(), 1), "测试Initialize参数Assembly");
        }


        public static Dictionary<string, bool> _assert = new Dictionary<string, bool>()
        {
            { "COM初始化执行",false },
            { "COM切换时执行",false },
            { "Ribbon初始化执行",false },
            { "Ribbon切换时执行",false },
            { "Idle初始化执行",false },
        };

        [ComMenuInitialize]
        public void COM_m()
        {
            if (!Init.IsLoaded)
            {
                _assert["COM初始化执行"] = true;
            }
            else
            {
                _assert["COM切换时执行"] = true;
            }
        }

        [RibbonInitialize]
        public void Ribbon_m()
        {
            if (!Init.IsLoaded)
            {
                _assert["Ribbon初始化执行"] = true;
            }
            else
            {
                _assert["Ribbon切换时执行"] = true;
            }
        }

        [IdleInitialize]
        public void Idle_m()
        {
            if (Init.IsLoaded)
            {
                //初始化结束时执行
                _assert["Idle初始化执行"] = true;
            }
        }

        [Test(Description = "测试InitializeAttribute的派生类")]
        public void NunitTestInitialize()
        {

            foreach (KeyValuePair<string, bool> item in _assert)
            {
                Assert.IsTrue(item.Value, item.Key);
            }
        }
    }
}
