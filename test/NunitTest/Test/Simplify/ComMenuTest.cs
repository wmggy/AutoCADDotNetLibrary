﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "4.1.Simplify.ComMenu")]
    public class ComMenuTest
    {
        [AcadPopupMenuItem("NunitComMenu1", 1, Check = true, Enable = false, HelpString = "123")]
        [CommandMethod("NunitComMenu1")]
        public void NunitComMenu1()
        {
        }

        [AcadPopupMenuItem("NunitComMenu2", 2)]
        [CommandMethod("NunitComMenu2")]
        public void NunitComMenu2()
        {
        }

        private static dynamic _comMenu = null;
        private static dynamic _items = null;
        [Test(Description = "测试ComMenu")]
        public void NunitComMenu()
        {
            dynamic acadApp = Application.AcadApplication;
            if (_comMenu == null)
            {
                _comMenu = acadApp.MenuGroups.Item(0).Menus.Add("NunitComMenu");

                //添加功能
                _items = AcadPopupMenuItemAtt.AddAcadPopupMenuItem(_comMenu, typeof(ComMenuTest));
            }

            //测试
            Assert.IsTrue(_items[0].Check == true &&
                _items[0].Enable == false &&
                _items[0].HelpString == "123" &&
                _items[0].Label == "NunitComMenu1" &&
                _items[0].Macro.Contains("NunitComMenu1"), "AcadPopupMenuItem1测试");

            Assert.IsTrue(_items[1].Check == false &&
                _items[1].Enable == true &&
                _items[1].Label == "NunitComMenu2" &&
                _items[1].Macro.Contains("NunitComMenu2"), "AcadPopupMenuItem2测试");
        }


    }
}
