﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "2.3.Extention.Editor")]
    public class EditorTest
    {
        public static Dictionary<string, bool> _assert = new Dictionary<string, bool>()
        {
            { "窗口视图内测试SelectWindow",false },
            { "窗口视图外测试SelectWindow",false },
        };

        [CommandMethod("NunitEditor")]
        public void NunitEditor()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ViewTableRecord view = doc.Editor.GetCurrentView();
            Point3d ptMin = new Point3d(view.CenterPoint.X - view.Width / 2, view.CenterPoint.Y - view.Height / 2, 0);
            Point3d ptMax = new Point3d(view.CenterPoint.X + view.Width / 2, view.CenterPoint.Y + view.Height / 2, 0);

            Point3d ptMinL = new Point3d(view.CenterPoint.X - view.Width / 3, view.CenterPoint.Y - view.Height / 3, 0);
            Point3d ptMaxL = new Point3d(view.CenterPoint.X + view.Width / 3, view.CenterPoint.Y + view.Height / 3, 0);

            ObjectId id = default;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Line l = new Line(ptMinL, ptMaxL);
                doc.Database.AddEntityToSpace(Space.CurrentSpace, false, l);
                id = l.Id;

                trans.Commit();
            }

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                List<Entity> laypsr = doc.Editor.SelectWindow(ptMin, ptMax).GetEntityByOK(OpenMode.ForWrite).ToList();
                if (laypsr.FirstOrDefault(x => x.Id == id) != null)//只有在视口内的SelectWindow能得到正确的答案
                {
                    _assert["窗口视图内测试SelectWindow"] = true;
                }
            }


            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                doc.Editor.SetZoomWSynchronize(new Point3d[] { new Point3d(ptMin.X - 100, ptMin.Y - 100, 0), ptMin });
                List<Entity> laypsr = doc.Editor.SelectWindow(ptMin, ptMax).GetEntityByOK(OpenMode.ForWrite).ToList();
                if (laypsr.FirstOrDefault(x => x.Id == id) == null)//只有在视口放大时，SelectWindow得到错误的答案
                {
                    _assert["窗口视图外测试SelectWindow"] = true;
                }
            }
        }

        [Test(Description = "测试Editor")]
        public void NunitTestInitialize()
        {
            foreach (KeyValuePair<string, bool> item in _assert)
            {
                Assert.IsTrue(item.Value, item.Key);
            }
        }
    }
}
