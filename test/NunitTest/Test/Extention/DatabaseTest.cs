﻿using System.IO;
using System.Linq;
using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "2.2.Extention.Database")]
    public class DatabaseTest
    {
        [Test(Description = "测试Database")]
        public void NunitDatabase()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = Application.DocumentManager.GetDocument(db);

            using (doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    Line l = new Line(new Point3d(0, 0, 0), new Point3d(100, 100, 0));

                    DBText t = new DBText();
                    t.TextString = "21312321";
                    t.SetCenteredMode(new Point3d(100, 100, 0));

                    db.AddEntityToSpace(Space.CurrentSpace, false, t, l);

                    string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\SplitCurve.dwg";
                    string newpath1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\TempDatabase1.dwg";
                    string newpath2 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\TempDatabase2.dwg";
                    db.OutputNewDWGFile(new Entity[] { t, l }, newpath1);
                    db.CopyToDWGFile(path, new Entity[] { t, l }, newpath2);


                    int count = db.ImportToEntitySetFromDwg(path, new Point3d()).Count();
                    Assert.IsTrue(count != 0, "测试导入dwg实体");

                    DatabaseExtention.OperateDWGFile(newpath1, null, ents =>
                    {
                        Assert.IsTrue(ents.OfType<DBText>().Any(x => t.TextString == "21312321"), "测试OutputNewDWGFile");
                    });

                    DatabaseExtention.OperateDWGFile(newpath2, null, ents =>
                    {
                        Assert.IsTrue(ents.OfType<DBText>().Any(x => t.TextString == "21312321"), "测试CopyToDWGFile");
                    });
                    File.Delete(newpath1);
                    File.Delete(newpath2);
                }
            }

        }
    }
}
