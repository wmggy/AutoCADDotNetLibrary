﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "2.6.ExtentionTable")]
    public class TableTest
    {
        [Test(Description = "测试Table")]
        public void NunitTable()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = Application.DocumentManager.GetDocument(db);

            using (doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    LayerTableRecord t1 = doc.Database.AddTableRecord<LayerTableRecord>("temp图层");
                    LayerTableRecord t2 = doc.Database.GetTableRecord<LayerTableRecord>("temp图层", OpenMode.ForRead);
                    IEnumerable<LayerTableRecord> all = doc.Database.GetTableRecords<LayerTableRecord>(OpenMode.ForRead);

                    bool a1 = t1.IsCanDelete();
                    bool a2 = all.FirstOrDefault(x => x.Name == "0")?.IsCanDelete() ?? true;

                    IEnumerable<SymbolTableRecord> removeList = all.CanDeleteList();

                    Assert.IsTrue(t1 == t2, "测试AddTableRecord成功");
                    Assert.IsTrue(all.Contains(t1), "测试GetTableRecords成功");
                    Assert.IsTrue(a1 = true && a2 == false, "测试IsCanDelete成功");
                    Assert.IsTrue(removeList.Contains(t1), "测试CanDeleteList成功");
                }
            }
        }
    }
}
