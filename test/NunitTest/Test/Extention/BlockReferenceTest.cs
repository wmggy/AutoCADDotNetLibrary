﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "2.1.Extention.BlockReference")]
    public class BlockReferenceTest
    {
        [Test(Description = "测试BlockReference")]
        public void NunitBlockReference()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = Application.DocumentManager.GetDocument(db);

            using (doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    //块定义
                    Circle circle = new Circle() { Center = Point3d.Origin, Radius = 100, ColorIndex = 1 };
                    AttributeDefinition att1 = new AttributeDefinition(Point3d.Origin, "", "x坐标", "", ObjectId.Null)
                    {
                        Invisible = true
                    };
                    AttributeDefinition att2 = new AttributeDefinition(Point3d.Origin, "", "y坐标", "", ObjectId.Null)
                    {
                        Invisible = true
                    };

                    BlockTableRecord btr = db.AddBlockTableRecord("块", circle, att1, att2);

                    //属性块
                    BlockReference bref = new BlockReference(Point3d.Origin, btr.ObjectId);
                    List<KeyValuePair<string, string>> data1 = new List<KeyValuePair<string, string>>()
                    {
                         new KeyValuePair<string, string>( "x坐标", "1000" ),
                         new KeyValuePair<string, string>( "y坐标", "200" ),
                    };

                    db.AddEntityToSpace(Space.CurrentSpace, false, bref);

                    //添加
                    bref.AddAttributes(data1);
                    List<KeyValuePair<string, string>> getData1 = bref.GetAttributes();
                    Assert.IsTrue(getData1.SequenceEqual(data1), "测试属性块添加成功");

                    //更新
                    List<KeyValuePair<string, string>> data2 = new List<KeyValuePair<string, string>>()
                    {
                         new KeyValuePair<string, string>( "x坐标", "1001" ),
                         new KeyValuePair<string, string>( "y坐标", "201" ),
                    };

                    bref.UpdateAttributes(data2);
                    List<KeyValuePair<string, string>> getData2 = bref.GetAttributes();
                    Assert.IsTrue(getData2.SequenceEqual(data2), "测试属性块添加成功");
                }
            }
        }
    }
}
