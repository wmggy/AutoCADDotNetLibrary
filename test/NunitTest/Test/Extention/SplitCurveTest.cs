﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "2.5.Extention.SplitCurve")]
    public class SplitCurveTest
    {
        /// <summary>
        /// <see cref="CurveExtention.GetSplitCurvesByPoints(Curve, Point3d[])"/>
        /// </summary>
        /// <param name="thisCurve"></param>
        /// <param name="pts"></param>
        /// <returns></returns>
        [DebuggerHidden]
        public static IEnumerable<Curve> GetSplitCurvesByPoints(Curve thisCurve, Point3d[] pts)
        {
            if (pts.Length == 0)
                return Enumerable.Empty<Curve>();

            double[] ptList = pts.Where(x => thisCurve.IsPointOnCurve(x))
                 .Select(x => thisCurve.GetParameterAtPoint(x))
                 .OrderBy(x => x).ToArray();
            if (ptList.Length == 0)
            {
                return Enumerable.Empty<Curve>();
            }

            return thisCurve.GetSplitCurves(new DoubleCollection(ptList)).Cast<Curve>().ToList();
        }

        [Test(Description = "原生SplitCurve的测试")]
        public void GetSplitCurvesByPoints()
        {
            string fileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\SplitCurve.dwg";

            //测试中发现的错误，解决方案是当为1个点时不切割圆，样条曲线，椭圆
            Action<Database> act = database =>
            {
                //1个点切不出2个圆不是2个
                DBPoint pt1 = (DBPoint)database.GetEntityByHandle(OpenMode.ForRead, 665);
                Circle circle = (Circle)database.GetEntityByHandle(OpenMode.ForRead, 630);

                IEnumerable<Curve> cs1 = GetSplitCurvesByPoints(circle, new Point3d[] { pt1.Position });

                Assert.IsTrue(cs1.Count() == 1, "原生测试：圆");


                //报错,1个点分割样条曲线
                Assert.Throws<Autodesk.AutoCAD.Runtime.Exception>([DebuggerHidden] () =>
                {
                    DBPoint pt2 = (DBPoint)database.GetEntityByHandle(OpenMode.ForRead, 664);
                    Spline spline = (Spline)database.GetEntityByHandle(OpenMode.ForRead, 635);

                    IEnumerable<Curve> cs2 = GetSplitCurvesByPoints(spline, new Point3d[] { pt2.Position });
                }, "原生测试：样条曲线");

                //报错,1个点分割椭圆
                Assert.Throws<Autodesk.AutoCAD.Runtime.Exception>([DebuggerHidden] () =>
                {
                    DBPoint pt3 = (DBPoint)database.GetEntityByHandle(OpenMode.ForRead, 666);
                    Ellipse ellipse = (Ellipse)database.GetEntityByHandle(OpenMode.ForRead, 636);

                    IEnumerable<Curve> cs3 = GetSplitCurvesByPoints(ellipse, new Point3d[] { pt3.Position });
                }, "原生测试：椭圆");
            };

            DatabaseExtention.OperateDWGFile(fileName, act, null);
        }

        [Test(Description = "修改SplitCurve后的测试")]
        public void GetSplitCurvesByPointsByModify()
        {
            string fileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\SplitCurve.dwg";

            //测试中发现的错误，解决方案是当为1个点时不切割圆，样条曲线，椭圆
            Action<Database> act = database =>
            {
                //圆
                DBPoint pt1 = (DBPoint)database.GetEntityByHandle(OpenMode.ForRead, 665);
                Circle circle = (Circle)database.GetEntityByHandle(OpenMode.ForRead, 630);

                IEnumerable<Curve> cs1 = circle.GetSplitCurvesByPoints(new Point3d[] { pt1.Position });
                Assert.IsTrue(cs1.Count() == 0, "修改后的测试：圆");

                //样条曲线
                DBPoint pt2 = (DBPoint)database.GetEntityByHandle(OpenMode.ForRead, 664);
                Spline spline = (Spline)database.GetEntityByHandle(OpenMode.ForRead, 635);

                IEnumerable<Curve> cs2 = spline.GetSplitCurvesByPoints(new Point3d[] { pt2.Position });
                Assert.IsTrue(cs2.Count() == 0, "修改后的测试：样条曲线");

                //椭圆
                DBPoint pt3 = (DBPoint)database.GetEntityByHandle(OpenMode.ForRead, 666);
                Ellipse ellipse = (Ellipse)database.GetEntityByHandle(OpenMode.ForRead, 636);

                IEnumerable<Curve> cs3 = ellipse.GetSplitCurvesByPoints(new Point3d[] { pt3.Position });
                Assert.IsTrue(cs3.Count() == 0, "修改后的测试：椭圆");

            };

            DatabaseExtention.OperateDWGFile(fileName, act, null);
        }
    }
}
