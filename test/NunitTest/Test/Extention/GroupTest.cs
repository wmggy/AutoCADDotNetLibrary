﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using NUnit.Framework;


namespace NunitTest
{
    [TestFixture(TestName = "2.4.Extention.Group")]
    public class GroupTest
    {
        [Test(Description = "测试Group")]
        public void NunitGroup()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = Application.DocumentManager.GetDocument(db);

            using (doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    Entity[] ents = Enumerable.Range(1, 10).Select(x => new Line(Point3d.Origin, new Point3d(x * 100, x * 100, 0))).Cast<Entity>().ToArray();
                    doc.Database.AddEntityToSpace(Space.CurrentSpace, false, ents);

                    Group a0_1 = doc.Database.AddGroup("A", ents[0].Id);
                    Group a1_1 = doc.Database.AddSeriesNumGroup("A", ents[1].Id);
                    Group a2_1 = doc.Database.AddSeriesNumGroup("A", ents[2].Id);

                    Group a0_2 = doc.Database.GetGroup("A", OpenMode.ForRead);
                    Group a1_2 = doc.Database.GetGroup("A1", OpenMode.ForRead);
                    Group a2_2 = doc.Database.GetGroup("A2", OpenMode.ForRead);

                    doc.Database.RemoveGroup("A");
                    Group a0_3 = doc.Database.GetGroup("A", OpenMode.ForRead);

                    IEnumerable<Group> a1_3 = ents[1].GetGroups(OpenMode.ForRead);
                    IEnumerable<Group> all = doc.Database.GetGroups(OpenMode.ForRead);
                    Assert.IsTrue(a0_1 == a0_2, "测试AddGroup成功");
                    Assert.IsTrue(a1_1 == a1_2 && a2_1 == a2_2, "测试AddSeriesNumGroup成功");
                    Assert.IsTrue(a0_3 == null, "测试RemoveGroup成功");
                    Assert.IsTrue(a1_3.Contains(a1_1), "测试obj.GetGroups成功");
                    Assert.IsTrue(all.Contains(a1_1) && all.Contains(a2_1), "测试db.GetGroups成功");
                }
            }
        }
    }
}
