﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using NUnit.Framework;

namespace NunitTest
{
    [TestFixture(TestName = "1.1.Event.ReadEntityEvent")]
    public static class ReadEntityEventTest
    {
        public class ReadEntityEvent : IReadEntityEvent
        {
            public bool IsUseEvent { get; set; } = true;

            public void ReadEntities(Document doc, Entity[] ents)
            {
                Count = ents.Length;
            }

            public int Count { get; set; }
        }

        public static Dictionary<string, bool> _assert = new Dictionary<string, bool>()
        {
            { "SelectEntityEvent的ReadEntityEvent.ReadEntities执行成功",false },
            { "SelectEntityEvent的ReadEntityEvent.IsUseEvent执行成功",false },
            { "SelectEntityEvent.移除执行成功",false },
            { "SelectEntityEvent.恢复执行成功",false },

            { "DocumentBecameCurrentEvent的ReadEntityEvent.ReadEntities执行成功",false },
            { "DocumentBecameCurrentEvent的ReadEntityEvent.IsUseEvent执行成功",false },
            { "DocumentBecameCurrentEvent.移除执行成功",false },
            { "DocumentBecameCurrentEvent.恢复执行成功",false },
        };

        private static ReadEntityEvent _see;
        private static ReadEntityEvent _dbc;

        [Initialize]
        public static void AddEvent()
        {
            _see = new ReadEntityEvent();
            _dbc = new ReadEntityEvent();

            SelectEntityEvent.Add(nameof(_see), _see);
            DocumentBecameCurrentEvent.Add(nameof(_dbc), _dbc);
        }

        public static ObjectId[] ids = null;
        [IdleInitialize]
        public static void SelectEntityEventTest()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = Application.DocumentManager.GetDocument(db);

            using (doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    Entity[] ents = Enumerable.Range(1, 10).Select(x => new Line(Point3d.Origin, new Point3d(x * 100, x * 100, 0))).Cast<Entity>().ToArray();
                    doc.Database.AddEntityToSpace(Space.CurrentSpace, false, ents);

                    ids = ents.Select(x => x.Id).ToArray();
                    trans.Commit();
                }
            }

            doc.Editor.SetImpliedSelection(ids);
            if (_see.Count != 0)
            {
                _assert["SelectEntityEvent的ReadEntityEvent.ReadEntities执行成功"] = true;
            }

            doc.Editor.SetImpliedSelection(new ObjectId[] { });
            _see.IsUseEvent = false;
            doc.Editor.SetImpliedSelection(ids);
            _see.IsUseEvent = true;
            if (_see.Count == 0)
            {
                _assert["SelectEntityEvent的ReadEntityEvent.IsUseEvent执行成功"] = true;
            }

            doc.Editor.SetImpliedSelection(new ObjectId[] { });
            SelectEntityEvent.Remove(nameof(_see));
            doc.Editor.SetImpliedSelection(ids);
            if (_see.Count == 0)
            {
                _assert["SelectEntityEvent.移除执行成功"] = true;
            }

            doc.Editor.SetImpliedSelection(new ObjectId[] { });
            SelectEntityEvent.Add(nameof(_see));
            doc.Editor.SetImpliedSelection(ids);
            if (_see.Count != 0)
            {
                _assert["SelectEntityEvent.恢复执行成功"] = true;
            }
        }


        private static string temp1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\temp1.dwg";
        private static string temp2 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\temp2.dwg";
        private static string temp3 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\temp3.dwg";
        private static string temp4 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\dwg\\temp4.dwg";

        //用debug调试时，总是报错System.AccessViolationException内存已损害，注释上下一行的代码就没问题。
        //当直接运行时没此问题。
        [IdleInitialize(Index = 1)]
        public static void DocumentBecameCurrentEventTest()
        {
            Document tempDoc = Application.DocumentManager.MdiActiveDocument;

            Application.DocumentManager.Open(temp1);
            if (_dbc.Count != 0)
            {
                _assert["DocumentBecameCurrentEvent的ReadEntityEvent.ReadEntities执行成功"] = true;
            }

            _dbc.Count = 0;
            _dbc.IsUseEvent = false;
            Application.DocumentManager.Open(temp2);
            _dbc.IsUseEvent = true;
            if (_dbc.Count == 0)
            {
                _assert["DocumentBecameCurrentEvent的ReadEntityEvent.IsUseEvent执行成功"] = true;
            }

            DocumentBecameCurrentEvent.Remove(nameof(_dbc));
            Application.DocumentManager.Open(temp3);
            if (_dbc.Count == 0)
            {
                _assert["DocumentBecameCurrentEvent.移除执行成功"] = true;
            }

            DocumentBecameCurrentEvent.Add(nameof(_dbc));
            Application.DocumentManager.Open(temp4);
            if (_dbc.Count != 0)
            {
                _assert["DocumentBecameCurrentEvent.恢复执行成功"] = true;
            }

            Application.DocumentManager.MdiActiveDocument = tempDoc;
        }


        [Test(Description = "测试ReadEntityEvent")]
        public static void NunitTestInitialize()
        {
            foreach (KeyValuePair<string, bool> item in _assert)
            {
                Assert.IsTrue(item.Value, item.Key);
            }
        }
    }
}
