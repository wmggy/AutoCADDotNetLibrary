﻿using System;
using Autodesk.AutoCAD.DatabaseServices;

namespace NunitTest
{
    public static class Common
    {
        public static DBObject GetEntityByHandle(this Database db, OpenMode openMode, long value)
        {
            bool isSuccess = db.TryGetObjectId(new Handle(value), out ObjectId objectId);
            if (!isSuccess)
            {
                throw new Exception("Handle值错误");
            }
            return objectId.GetObject(openMode);
        }
    }
}
