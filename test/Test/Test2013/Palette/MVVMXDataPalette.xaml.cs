﻿using System.Windows.Controls;

namespace Test
{
    /// <summary>
    /// MVVMXDataPalette.xaml 的交互逻辑
    /// </summary>
    public partial class MVVMXDataPalette : UserControl
    {
        public MVVMXDataPalette()
        {
            InitializeComponent();
        }

        public MVVMXDataPaletteViewModel ViewModel { get; } = new MVVMXDataPaletteViewModel();

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }
    }
}
