﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;

namespace Test
{
    public class Palette
    {
        private static PaletteSet ps = null;
        private static XDataPalette palette1;
        private static MVVMXDataPalette palette2;

        public void ShowPalette(string name)
        {
            if (ps == null)//如果面板没有被创建
            {
                ps = new PaletteSet("面板");

                palette1 = new XDataPalette();
                ps.AddVisual("XData面板", palette1, true);
                SelectEntityEvent.Add(nameof(XDataPalette), palette1.ViewModel);
                SelectEntityEvent.SetStateChanged(ps, nameof(XDataPalette));
                DocumentBecameCurrentEvent.Add(nameof(XDataPalette), palette1.ViewModel);
                DocumentBecameCurrentEvent.SetStateChanged(ps, nameof(XDataPalette));

                palette2 = new MVVMXDataPalette();
                ps.AddVisual("MVVM面板", palette2, true);
                SelectEntityEvent.Add(nameof(MVVMXDataPalette), palette2.ViewModel);
                SelectEntityEvent.SetStateChanged(ps, nameof(MVVMXDataPalette));
                DocumentBecameCurrentEvent.Add(nameof(MVVMXDataPalette), palette2.ViewModel);
                DocumentBecameCurrentEvent.SetStateChanged(ps, nameof(MVVMXDataPalette));
            }

            //激活UI
            for (int i = 0; i < ps.Count; i++)
            {
                if (ps[i].Name == name)
                    ps.Activate(i);
            }

            ps.Visible = true;//面板可见
        }



        [Initialize]
        [PopMenuItem(1), MenuMacro("XData面板")]
        [AcadPopupMenuItem("XData面板", 1)]
        [CommandMethod("XDataPalette1")]
        public void XDataPalette1()
        {
            ShowPalette("XData面板");
        }

        [PopMenuItem(2), MenuMacro("MVVM面板")]
        [AcadPopupMenuItem("MVVM面板", 2)]
        [CommandMethod("XDataPalette2")]
        public void XDataPalette2()
        {
            ShowPalette("MVVM面板");
        }
    }
}
