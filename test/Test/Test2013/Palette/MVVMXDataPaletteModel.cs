﻿using System.ComponentModel;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;

namespace Test
{
    [RegAppORM("Model")]
    public class MVVMXDataPaletteModel : INotifyPropertyChanged
    {
        public MVVMXDataPaletteModel()
        {
            PropertyChanged += (sender, e) =>
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                if (doc.Database != Id.Database || !Id.IsValid)
                    return;
                using (doc.LockDocument())
                {
                    using (Transaction trans = doc.TransactionManager.StartTransaction())
                    {
                        DBObject ent = Id.GetObject(OpenMode.ForWrite, false, true);
                        ent.SetXdataByModel<MVVMXDataPaletteModel>(this);

                        trans.Commit();
                    }
                }
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [XDataORM(0)]
        public string Name { get; set; }

        [XDataORM(1)]
        public string Value { get; set; }

        public ObjectId Id { get; set; }

    }
}
