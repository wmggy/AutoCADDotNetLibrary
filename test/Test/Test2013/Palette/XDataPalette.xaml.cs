﻿using System.Windows.Controls;

namespace Test
{
    /// <summary>
    /// XDataPalette.xaml 的交互逻辑
    /// </summary>
    public partial class XDataPalette : UserControl
    {
        public XDataPalette()
        {
            InitializeComponent();
        }

        public XDataPaletteViewModel ViewModel { get; } = new XDataPaletteViewModel();

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ViewModel?.ChangeXDataCommand.Execute(null);
        }
    }
}
