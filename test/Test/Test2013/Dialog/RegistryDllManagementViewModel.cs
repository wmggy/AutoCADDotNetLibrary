﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using AutoCADDotNetLibrary;
using Microsoft.Win32;

namespace Test
{
    public class RegistryDllManagementViewModel : INotifyPropertyChanged
    {
        public RegistryDllManagementViewModel()
        {
            UpdateCommand.Execute(null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private string _searchValue;
        public string SearchValue
        {
            get => _searchValue;
            set
            {
                _searchValue = value;
                Search();
            }
        }
        public List<RegistryDllManagementModel> ALLModels { get; } = new List<RegistryDllManagementModel>();
        public ObservableCollection<RegistryDllManagementModel> FilterModels { get; } = new ObservableCollection<RegistryDllManagementModel>();

        public ICommand UpdateCommand => new RelayCommand(() =>
        {
            ALLModels.Clear();

            RegistryKey user = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Autodesk\\AutoCAD", false);
            string[] versions = user.GetSubKeyNames();

            var r = versions.Select(x =>
            {
                RegistryKey userT = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Autodesk\\AutoCAD\\" + x, false);
                string curVer = userT.GetValue("CurVer") as string;
                return new { CurVer = curVer, Location = $"SOFTWARE\\Autodesk\\AutoCAD\\{x}\\{curVer}\\Applications", Version = x };
            })
            .Where(x => !string.IsNullOrEmpty(x.CurVer))
            .SelectMany(x =>
            {
                RegistryKey userT1 = Registry.CurrentUser.OpenSubKey(x.Location, false);
                return userT1.GetSubKeyNames().Select(y =>
                {
                    string l = $"{x.Location}\\{y}";
                    RegistryKey userT2 = Registry.CurrentUser.OpenSubKey(l, false);
                    string path = userT2?.GetValue("LOADER") as string;

                    return new { Name = y, Version = x.Version, Location = l, Path = path };
                });
            })
            .ToList();

            r.ForEach(x => ALLModels.Add(new RegistryDllManagementModel(x.Name, x.Version, x.Location, x.Path)));

            Search();
        });

        public ICommand DeleteSelectModelCommand => new RelayCommand<IList>(x =>
        {
            MessageBoxResult r = MessageBox.Show("是否删除显示项的注册表？", "消息", MessageBoxButton.YesNo);
            if (r == MessageBoxResult.No)
                return;

            foreach (RegistryDllManagementModel item in x)
            {
                RegistryKey user = Registry.CurrentUser.OpenSubKey(System.IO.Path.GetDirectoryName(item.Location), true);
                user?.DeleteSubKey(System.IO.Path.GetFileName(item.Location), false);
            }

            UpdateCommand.Execute(null);
        });

        private void Search()
        {
            FilterModels.Clear();
            if (string.IsNullOrWhiteSpace(SearchValue))
            {
                ALLModels.ForEach(x => FilterModels.Add(x));
            }
            else
            {
                ALLModels.Where(x => x.ToString().Contains(SearchValue)).ToList().ForEach(x => FilterModels.Add(x));
            }
        }
    }
}
