﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class DialogCommand
    {
        [PopMenuItem(1), MenuMacro("管理注册表")]
        [AcadPopupMenuItem("管理注册表", 1)]
        [CommandMethod("ShowDialog")]
        public void ShowDialog()
        {
            Application.ShowModalWindow(new RegistryDllManagement());
        }
    }
}
