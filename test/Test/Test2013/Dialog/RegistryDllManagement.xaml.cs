﻿using System.Windows;
using System.Windows.Controls;

namespace Test
{
    /// <summary>
    /// RegistryDllManagement.xaml 的交互逻辑
    /// </summary>
    public partial class RegistryDllManagement : Window
    {
        public RegistryDllManagement()
        {
            InitializeComponent();
        }

        public RegistryDllManagementViewModel ViewModel { get; } = new RegistryDllManagementViewModel();

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }
    }
}
