﻿using System.Collections.Generic;

namespace Test
{
    public class RegistryDllManagementModel
    {
        public RegistryDllManagementModel(string name, string version, string location, string path)
        {
            Name = name;
            Version = version;
            Location = location;
            Path = path;

            if (_dic.ContainsKey(Version ?? ""))
                AutoCADVersion = _dic[Version];
        }

        public string Name { get; }
        public string AutoCADVersion { get; }
        public string Version { get; }
        public string Location { get; }
        public string Path { get; }

        public override string ToString()
        {
            return $"Name:{Name};AutoCADVersion:{AutoCADVersion};Version:{Version};Location:{Location};Path:{Path}";
        }

        private static Dictionary<string, string> _dic = new Dictionary<string, string>()
        {
            { "R16.2", "2006" },
            { "R17.0", "2007" },
            { "R17.1", "2008" },
            { "R17.2", "2009" },
            { "R18.0", "2010" },
            { "R18.1", "2011" },
            { "R18.2", "2012" },
            { "R19.0", "2013" },
            { "R19.1", "2014" },
            { "R20.0", "2015" },
            { "R20.1", "2016" },
            { "R21.0", "2017" },
            { "R22.0", "2018" },
            { "R23.0", "2019" },
            { "R23.1", "2020" },
            { "R24.0", "2021" },
            { "R24.1", "2022" },
            { "R24.2", "2023" },
        };
    }
}
