﻿using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class TestCommandClass
    {
        public TestCommandClass()
        {
            _selfProperty = _i++;
        }
        private static int _i = 0;

        private int _selfProperty { get; }

        [PopMenuItem(1, IsAddSeparatorAfter = true), MenuMacro("测试命令")]
        [AcadPopupMenuItem("测试命令", 1, IsAddSeparatorAfter = true)]
        [CommandMethod("TestCommand")]
        public void TestCommand()
        {
            //打开另一个dwg,测试_selfProperty,各自的dwg,有各自的属性
            Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage($"\n{_selfProperty}");
        }
    }

    public class CADCommand
    {

        [PopMenuItem(1), MenuMacro("1.   卸载所有命令")]
        [AcadPopupMenuItem("1.   卸载所有命令", 1)]
        [CommandMethod("RemoveCADCommand")]
        public void RemoveCADCommand()
        {
            Command.Remove(Assembly.GetExecutingAssembly(), typeof(CADCommand));
        }


        [PopMenuItem(2), MenuMacro("2.   加载所有命令")]
        [AcadPopupMenuItem("2.   加载所有命令", 2)]
        [CommandMethod("AddCADCommand")]
        public void AddCADCommand()
        {
            Command.Add(Assembly.GetExecutingAssembly(), typeof(CADCommand));
        }
    }
}
