﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class EntityTest
    {
        [PopMenuItem(1), MenuMacro("1.   插入属性块")]
        [AcadPopupMenuItem("1.   插入属性块", 1)]
        [CommandMethod("AttributeBlock")]
        public void AttributeBlock()
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                //块定义
                Circle circle = new Circle() { Center = Point3d.Origin, Radius = 100, ColorIndex = 1 };
                AttributeDefinition att1 = new AttributeDefinition(Point3d.Origin, "", "x坐标", "", ObjectId.Null)
                {
                    Invisible = true
                };
                AttributeDefinition att2 = new AttributeDefinition(Point3d.Origin, "", "y坐标", "", ObjectId.Null)
                {
                    Invisible = true
                };

                BlockTableRecord btr = doc.Database.AddBlockTableRecord("块", circle, att1, att2);

                //属性块
                BlockReference bref = new BlockReference(Point3d.Origin, btr.ObjectId);
                List<KeyValuePair<string, string>> data = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("x坐标", "1000"),
                    new KeyValuePair<string, string>("y坐标", "200"),
                };

                doc.Database.AddEntityToSpace(Space.CurrentSpace, false, bref);
                bref.AddAttributes(data);

                trans.Commit();
            }
        }

        [PopMenuItem(2), MenuMacro("2.   属性块的属性")]
        [AcadPopupMenuItem("2.   属性块的属性", 2)]
        [CommandMethod("BlockOfAttribute")]
        public void BlockOfAttribute()
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                BlockReference ent = doc.Editor.Get<Entity>("选择属性块", new Type[] { typeof(BlockReference) }) as BlockReference;
                if (ent == null)
                    return;

                string str = string.Join("\r\n", ent.GetAttributes().Select((x, index) => (index + 1) + "    " + x.Key + "    " + x.Value).ToArray());

                doc.Editor.WriteMessage("\n" + str);
                trans.Commit();
            }
        }

        [PopMenuItem(3), MenuMacro("3.   设置文本位置")]
        [AcadPopupMenuItem("3.   设置文本位置", 3)]
        [CommandMethod("SetBasePosion")]
        public void SetBasePosion()
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                DBText t = new DBText();
                t.TextString = "21312321";
                t.SetCenteredMode(new Point3d(100, 100, 0));

                doc.Database.AddEntityToSpace(t);
                trans.Commit();
            }
        }

        [PopMenuItem(4), MenuMacro("4.   打断曲线")]
        [AcadPopupMenuItem("4.   打断曲线", 4)]
        [CommandMethod("SplitCurve")]
        public void SplitCurve()
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Curve ent = doc.Editor.Get<Entity>("选择曲线", new Type[] { typeof(Curve) }) as Curve;
                if (ent == null)
                    return;

                KeywordParameter keyword = new KeywordParameter();
                keyword.Keywords.Add("E");
                keyword.Keywords.Default = "E";

                List<Point3d> pts = new List<Point3d>();
                while (true)
                {
                    object pt = doc.Editor.Get<Point3d>("选择打断点", keywords: keyword);
                    if (pt == null)
                        return;
                    if (keyword.StringResult == "E")
                        break;

                    pts.Add((Point3d)pt);
                }

                IEnumerable<Curve> r = ent.GetSplitCurvesByPoints(pts.ToArray());

                doc.Database.AddEntityToSpace(r.OfType<Entity>().ToArray());
                trans.Commit();
            }
        }


        [PopMenuItem(5), MenuMacro("5.   HandleValue")]
        [AcadPopupMenuItem("5.   HandleValue", 5)]
        [CommandMethod("HandleValue")]
        public void HandleValue()
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Entity ent = doc.Editor.Get<Entity>("选择实体") as Entity;
                if (ent == null)
                    return;

                doc.Editor.WriteMessage($"\n{ent.Handle.Value}\n");
            }
        }
    }
}
