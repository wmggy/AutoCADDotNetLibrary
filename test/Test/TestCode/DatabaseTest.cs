﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Test
{
    public class DatabaseTest
    {
        [PopMenuItem(1), MenuMacro("1.   切换dwg")]
        [AcadPopupMenuItem("1.   切换dwg", 1)]
        [CommandMethod("MdiActiveDocument", CommandFlags.Session)]
        public void MdiActiveDocument()
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Title = " 选择dwg文件 ",
                Filter = "CAD文件|*.dwg",
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            if (DatabaseExtention.IsFileInUse(dialog.FileName))
            {
                Application.ShowAlertDialog(dialog.FileName + "文件被占用");
                return;
            }

            Document doc = Application.DocumentManager.Open(dialog.FileName, false);

            using (doc.LockDocument())
            {
                using (Transaction trans = doc.TransactionManager.StartTransaction())
                {
                    DBText t = new DBText();
                    t.TextString = "asaaasas";
                    t.SetCenteredMode(new Point3d(100, 100, 0));

                    doc.Database.AddEntityToSpace(t);
                    trans.Commit();
                }
            }
        }

        [PopMenuItem(2), MenuMacro("2.   导入dwg到块定义中")]
        [AcadPopupMenuItem("2.   导入dwg到块定义中", 2)]
        [CommandMethod("ImportDwgToBlock")]
        public void ImportDwgToBlock()
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Title = " 选择dwg文件 ",
                Filter = "CAD文件|*.dwg",
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            if (DatabaseExtention.IsFileInUse(dialog.FileName))
            {
                Application.ShowAlertDialog(dialog.FileName + "文件被占用");
                return;
            }

            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                doc.Database.ImportToBlockFromDwg(dialog.FileName, Path.GetFileNameWithoutExtension(dialog.FileName));

                trans.Commit();
            }
        }

        [PopMenuItem(3), MenuMacro("3.   导入dwg实体到模型空间中")]
        [AcadPopupMenuItem("3.   导入dwg实体到模型空间中", 3)]
        [CommandMethod("ImportDwgToModelSpase")]
        public void ImportDwgToModelSpase()
        {
            //得到dwg文件
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Title = " 选择块文件 ",
                Filter = "CAD文件|*.dwg",
            };
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            if (DatabaseExtention.IsFileInUse(dialog.FileName))
            {
                Application.ShowAlertDialog(dialog.FileName + "文件被占用");
                return;
            }

            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                //得到实体
                List<Entity> ents = doc.Database.ImportToEntitySetFromDwg(dialog.FileName, Point3d.Origin).ToList();
                doc.Database.AddEntityToSpace(ents.ToArray());

                trans.Commit();
            }
        }

        private string GetNewFileName(string dir)
        {
            string file = dir + "\\New.dwg";

            int i = 1;
            while (File.Exists(file))
            {
                file = dir + $"\\New_{i++}.dwg";
            }

            return file;
        }

        [PopMenuItem(4), MenuMacro("4.   输出实体到空文件")]
        [AcadPopupMenuItem("4.   输出实体到空文件", 4)]
        [CommandMethod("OutputDwg")]
        public void OutputDwg()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog()
            {
                Description = "选择输出文件目录",
            };
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Line l = new Line(new Point3d(0, 0, 0), new Point3d(100, 100, 0));

                DBText t = new DBText();
                t.TextString = "21312321";
                t.SetCenteredMode(new Point3d(100, 100, 0));

                doc.Database.AddEntityToSpace(Space.CurrentSpace, false, t, l);
                trans.Commit();

                string file = GetNewFileName(dialog.SelectedPath);
                doc.Database.OutputNewDWGFile(new Entity[] { t, l }, file);
            }
        }

        [PopMenuItem(5), MenuMacro("5.   输出实体到已有文件")]
        [AcadPopupMenuItem("5.   输出实体到已有文件", 5)]
        [CommandMethod("CopyToDWGFile")]
        public void CopyToDWGFile()
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Title = " 选择dwg文件 ",
                Filter = "CAD文件|*.dwg",
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            if (DatabaseExtention.IsFileInUse(dialog.FileName))
            {
                Application.ShowAlertDialog(dialog.FileName + "文件被占用");
                return;
            }

            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Line l = new Line(new Point3d(0, 0, 0), new Point3d(100, 100, 0));

                DBText t = new DBText();
                t.TextString = "21312321";
                t.SetCenteredMode(new Point3d(100, 100, 0));

                doc.Database.AddEntityToSpace(Space.CurrentSpace, false, t, l);
                trans.Commit();

                string saveFile = GetNewFileName(Path.GetDirectoryName(dialog.FileName));
                doc.Database.CopyToDWGFile(dialog.FileName, new Entity[] { t, l }, saveFile);
            }
        }

        //出现的问题：DBText.SetCenteredMode()会无效化
        //具体的原因：看文档DBText.AdjustAlignment(Database alternateDatabaseToUse);
        //具体的解决方案是"4.   输出实体到空文件"，先在工作的Database生成数据DBText，然后再导出。而不是生成DBText直接导出。下面是错误案例。
        [PopMenuItem(6), MenuMacro("6.   写入dwg->问题：添加txt")]
        [AcadPopupMenuItem("6.   写入dwg->问题：添加txt", 6)]
        [CommandMethod("OperatorDwg")]
        public void OperatorDwg()
        {
            //得到dwg文件
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Title = " 选择dwg文件 ",
                Filter = "CAD文件|*.dwg",
            };
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            if (DatabaseExtention.IsFileInUse(dialog.FileName))
            {
                Application.ShowAlertDialog(dialog.FileName + "文件被占用");
                return;
            }

            DatabaseExtention.OperateDWGFile(dialog.FileName, x =>
            {
                Line l = new Line(new Point3d(0, 0, 0), new Point3d(100, 100, 0));

                DBText t = new DBText();
                t.TextString = "21312321";
                t.SetCenteredMode(new Point3d(100, 100, 0));

                x.AddEntityToSpace(t, l);
            }, null, true, GetNewFileName(Path.GetDirectoryName(dialog.FileName)));
        }
    }
}