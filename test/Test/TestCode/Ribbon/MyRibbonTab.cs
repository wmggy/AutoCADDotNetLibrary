﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Customization;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Test
{
    public partial class MyRibbonTab
    {
        public MyRibbonTab()
        {
            InitializeComponent();
        }

        public static readonly MyRibbonTab Instance = new MyRibbonTab();

        [RibbonInitialize]
        [RibbonButton("添加Ribbon", 0, RibbonButtonStyle = RibbonButtonStyle.LargeWithHorizontalText)]
        [MenuMacro("添加Ribbon", HelpString = "添加Ribbon_HelpString")]
        [CommandMethod("AddRibbonXAML")]
        public static void AddRibbonXAML()
        {
            if (!ComponentManager.Ribbon.Tabs.Contains(Instance))
            {
                ComponentManager.Ribbon.Tabs.Add(Instance);
            }
        }

        [RibbonButton("直线", 1, RibbonButtonStyle = RibbonButtonStyle.LargeWithHorizontalText,
            SmallImage = "B3.ico", LargeImage = "pack://application:,,,/Test;component/A3.ico")]
        [MenuMacro("直线l31", HelpString = "直线l31_HelpString", SmallImageFileName = "CUIImage/B2.ico", LargeImageFileName = "CUIImage/A2.ico")]
        [CommandMethod("l31")]
        public void l31()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }

#if !AC2010
        [RibbonButton("多段线", 2, RibbonButtonStyle = RibbonButtonStyle.LargeWithoutText, SmallImage = "B3.ico", LargeImage = "A3.ico")]
        [MenuMacro("直线l32", HelpString = "直线l32_HelpString", SmallImageFileName = "CUIImage/B2.ico", LargeImageFileName = "CUIImage/A2.ico")]
        [CommandMethod("l32")]
        public void l32()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }
#endif

        [RibbonButton("直线", 3, RibbonButtonStyle = RibbonButtonStyle.LargeWithText, SmallImage = "B3.ico", LargeImage = "A3.ico")]
        [MenuMacro("直线l33", HelpString = "直线l33_HelpString", SmallImageFileName = "CUIImage/B2.ico", LargeImageFileName = "CUIImage/A2.ico")]
        [CommandMethod("l33")]
        public void l33()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }

        [RibbonButton("直线", 4, RibbonButtonStyle = RibbonButtonStyle.SmallWithoutText, SmallImage = "B3.ico", LargeImage = "A3.ico")]
        [MenuMacro("直线l34", HelpString = "直线l34_HelpString", SmallImageFileName = "CUIImage/B2.ico", LargeImageFileName = "CUIImage/A2.ico")]
        [CommandMethod("l34")]
        public void l34()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }

        [RibbonButton("直线", 5, RibbonButtonStyle = RibbonButtonStyle.SmallWithText, SmallImage = "B3.ico", LargeImage = "A3.ico")]
        [MenuMacro("直线l35", HelpString = "直线l35_HelpString", SmallImageFileName = "CUIImage/B2.ico", LargeImageFileName = "CUIImage/A2.ico")]
        [CommandMethod("l35")]
        public void l35()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }
    }
}
