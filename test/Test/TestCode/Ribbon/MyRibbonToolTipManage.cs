﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;

namespace Test
{
    public class MyRibbonToolTipManage : RibbonToolTipManage
    {
        public override RibbonToolTip CreateRibbonToolTip(CommandMethodAttribute commandMethod, RibbonButtonAttribute ribbonButton)
        {
            RibbonToolTip toolTip = base.CreateRibbonToolTip(commandMethod, ribbonButton);
            toolTip.Content = $"Content:{commandMethod.GlobalName}";
            toolTip.ExpandedContent = $"ExpandedContent:{commandMethod.GlobalName}";

            return toolTip;
        }

        [Initialize(Index = -1000)]//要先于new MyRibbonTab()使用
        public static void init()
        {
            RibbonToolTipManage.Instance = new MyRibbonToolTipManage();
        }
    }
}
