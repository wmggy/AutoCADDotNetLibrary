﻿using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class Init : IExtensionApplication
    {
        public void Initialize()
        {
            ExtensionApplication.Initialize(Assembly.GetExecutingAssembly());
        }

        public void Terminate()
        {
            ExtensionApplication.Terminate(Assembly.GetExecutingAssembly());
        }
    }
}