﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class AOP
    {
        [PopMenuItem(1), MenuMacro("AOP")]
        [AcadPopupMenuItem("AOP", 1)]
        [CommandMethod("AOP")]
        public void method()
        {
            SimpleAutoCADAOP.RunCommandMethod(_method);
        }

        private void _method(Document doc, Transaction trans)
        {
            Line l = new Line(new Point3d(0, 0, 0), new Point3d(100, 100, 0));
            doc.Database.AddEntityToSpace(l);
        }
    }
}
