﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Customization;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class LayerTest
    {
        [PopMenuItem(1), MenuMacro("1.   隐藏实体图层")]
        [AcadPopupMenuItem("1.   隐藏实体图层", 1)]
        [CommandMethod("HideEntity")]
        public void HideEntity()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Entity ent = doc.Editor.Get<Entity>("选择实体") as Entity;
                if (ent == null)
                    return;

                doc.Database.GetTableRecord<LayerTableRecord>(ent.Layer, OpenMode.ForWrite).IsOff = true;

                trans.Commit();
            }
        }

        [PopMenuItem(2, IsAddSeparatorAfter = true), MenuMacro("2.   只显示实体图层")]
        [AcadPopupMenuItem("2.   只显示实体图层", 2, IsAddSeparatorAfter = true)]
        [CommandMethod("OnlyHideEntity")]
        public void OnlyHideEntity()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Entity ent = doc.Editor.Get<Entity>("选择实体") as Entity;
                if (ent == null)
                    return;

                doc.Database.OperatorAllLayerTableRecord(true, null, null);
                doc.Database.GetTableRecord<LayerTableRecord>(ent.Layer, OpenMode.ForWrite).IsOff = false;

                trans.Commit();
            }
        }


        [PopMenuItem(3), MenuMacro("1.   添加1~10图层")]
        [AcadPopupMenuItem("1.   添加1~10图层", 3)]
        [CommandMethod("AddLayer")]
        public void AddLayer()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Line[] ents = Enumerable.Range(1, 9).Select(x =>
                {
                    LayerTableRecord l = doc.Database.AddTableRecord<LayerTableRecord>(x.ToString());
                    l.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, (short)x);

                    Line line = new Line(new Point3d(0 + x * 100, 0, 0), new Point3d(100 + x * 100, 100, 0));
                    line.Layer = x.ToString();

                    return line;
                }).ToArray();

                doc.Database.AddEntityToSpace(Space.CurrentSpace, false, ents);
                trans.Commit();
            }
        }

        [PopMenuItem(4), MenuMacro("2.   清理所有空图层")]
        [AcadPopupMenuItem("2.   清理所有空图层", 4)]
        [CommandMethod("DeleteLayer")]
        public void DeleteLayer()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                List<LayerTableRecord> layers = doc.Database.GetTableRecords<LayerTableRecord>(OpenMode.ForWrite).Where(x => x.IsCanDelete()).ToList();
                layers.ForEach(x => x.Erase());

                trans.Commit();
            }
        }

        [PopMenuItem(5), MenuMacro("3.   打开所有图层")]
        [AcadPopupMenuItem("3.   打开所有图层", 5)]
        [CommandMethod("OpenAllLayer")]
        public void OpenAllLayer()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                doc.Database.OperatorAllLayerTableRecord(false, false, false);

                trans.Commit();
            }

            doc.Editor.Regen();
            //doc.SendStringToExecute("re\n", true, false, false);
        }

        [PopMenuItem(6), MenuMacro("4.  关闭所有图层")]
        [AcadPopupMenuItem("4.  关闭所有图层", 6)]
        [CommandMethod("CloseAllLayer")]
        public void CloseAllLayer()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                doc.Database.OperatorAllLayerTableRecord(true, true, true);
                trans.Commit();
            }
        }

    }

}