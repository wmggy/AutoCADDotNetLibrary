﻿using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class Regedit
    {
        //受信任目录
        //[Initialize]
        public void TRUSTEDPATHS()
        {
            Register.TrustePaths(Assembly.GetExecutingAssembly());
        }


        [PopMenuItem(1), MenuMacro("注册程序")]
        [AcadPopupMenuItem("注册程序", 1)]
        [CommandMethod("RegisteringCAD")]
        public void RegisteringCAD()
        {
            Register.RegisteringCAD(Assembly.GetExecutingAssembly());
        }

        [PopMenuItem(2), MenuMacro("取消注册")]
        [AcadPopupMenuItem("取消注册", 2)]
        [CommandMethod("DeleteRegisteringCAD")]
        public void DeleteRegisteringCAD()
        {
            Register.DeleteRegisteredCAD(Assembly.GetExecutingAssembly());
        }
    }
}