﻿using System;
using System.Collections.Generic;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class Jig
    {
        [PopMenuItem(1), MenuMacro("1.  Point3dJig")]
        [AcadPopupMenuItem("1.  Point3dJig", 1)]
        [CommandMethod("Point3dJig")]
        public void Point3dJig()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                Line l1 = new Line(Point3d.Origin, new Point3d(10, 10, 0));
                Line l2 = new Line(Point3d.Origin, new Point3d(-10, 10, 0));
                doc.Database.AddEntityToSpace(Space.CurrentSpace, false, l1, l2);

                Point3d lastPt = Point3d.Origin;
                Action<Point3d> act = x =>
                {
                    l1.TransformBy(Matrix3d.Displacement(x - lastPt));
                    l2.TransformBy(Matrix3d.Displacement(x - lastPt));
                    lastPt = x;
                };

                PromptResult resJig = doc.Editor.Drag(JigExtention.CreatePoint3dJig("V字", act, new List<Entity>() { l1, l2 }));
                if (resJig.Status == PromptStatus.Cancel)
                {
                    l1.Erase();
                    l2.Erase();
                }

                l1.Dispose();
                l2.Dispose();

                trans.Commit();
            }
        }

        [PopMenuItem(2), MenuMacro("2.  list的引用测试")]
        [AcadPopupMenuItem("2.  list的引用测试", 2)]
        [CommandMethod("Point3dJig1")]
        public void Point3dJig1()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                List<Entity> ents = new List<Entity>();

                DateTime now = DateTime.Now;
                Action<Point3d> act = x =>
                {
                    if ((DateTime.Now - now).TotalSeconds > 1)//5秒之后，并且动了，生成点，
                    {
                        DBPoint pt = new DBPoint(x);
                        ents.Add(pt);
                        doc.Database.AddEntityToSpace(Space.CurrentSpace, false, pt);

                        now = DateTime.Now;
                    }
                };

                PromptResult resJig = doc.Editor.Drag(JigExtention.CreatePoint3dJig("动态的添加点", act, ents));
                if (resJig.Status == PromptStatus.Cancel)
                {
                    ents.ForEach(x => x.Erase());
                }

                ents.ForEach(x => x.Dispose());

                trans.Commit();
            }
        }
    }
}
