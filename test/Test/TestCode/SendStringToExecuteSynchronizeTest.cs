﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class SendStringToExecuteSynchronizeTest
    {
        [PopMenuItem(1), MenuMacro("SendStringToExecute非同步问题")]
        [AcadPopupMenuItem("SendStringToExecute非同步问题", 1)]
        [CommandMethod("SendStringToExecuteAsynchronous")]
        public void SendStringToExecuteAsynchronous()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("filedia 0 ", true, false, false);
            doc.Editor.WriteMessage("\n" + "cw:filedia:" + Application.GetSystemVariable("filedia"));
            doc.SendStringToExecute("filedia 1 ", true, false, false);
            doc.Editor.WriteMessage("\n" + "cw:filedia:" + Application.GetSystemVariable("filedia"));
        }

        [PopMenuItem(2), MenuMacro("SendStringToExecute同步")]
        [AcadPopupMenuItem("SendStringToExecute同步", 2)]
        [CommandMethod("SendStringToExecuteSynchronize")]
        public void SendStringToExecuteSynchronize()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            doc.SendStringToExecute("filedia 0 ", true, false, false);
            doc.SendStringToExecuteSynchronizeMehtod(() =>
            {
                doc.Editor.WriteMessage("\n" + "cw:filedia:" + Application.GetSystemVariable("filedia"));
            });
            doc.SendStringToExecute("filedia 1 ", true, false, false);
            doc.SendStringToExecuteSynchronizeMehtod(() =>
            {
                doc.Editor.WriteMessage("\n" + "cw:filedia:" + Application.GetSystemVariable("filedia"));
            });
        }


        [PopMenuItem(3), MenuMacro("系统变量非同步问题")]
        [AcadPopupMenuItem("系统变量非同步问题", 3)]
        [CommandMethod("SystemVariableAsynchronous")]
        public void SystemVariableAsynchronous()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            Application.SetSystemVariable("filedia", 0);//同步
            doc.SendStringToExecute("netload " + ((char)27), true, false, false);//非同步，函数结束时运行
            Application.SetSystemVariable("filedia", 1);//同步
        }

        [PopMenuItem(4), MenuMacro("系统变量同步")]
        [AcadPopupMenuItem("系统变量同步", 4)]
        [CommandMethod("SystemVariableSynchronize")]
        public void SystemVariableSynchronize()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            SystemVariable.SetSystemVariable(SystemVariableType.NOMUTT, 1);
            SystemVariable.SetSystemVariable(SystemVariableType.FILEDIA, 0);

            doc.SendStringToExecute("netload " + ((char)27), true, false, false);

            SystemVariable.RestoreSystemVariable(doc);
        }
    }
}
