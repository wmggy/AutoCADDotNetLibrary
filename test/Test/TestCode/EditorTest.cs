﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class EditorTest
    {
        [CommandMethod("hello")]
        public void Hello()
        {
            Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("\nhello word");
        }

        [PopMenuItem(1), MenuMacro("1.   (全选)选中状态：lsp")]
        [AcadPopupMenuItem("1.   (全选)选中状态：lsp", 1)]
        [CommandMethod("SSetImpliedSelection1")]
        public void SetImpliedSelection1()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                IEnumerable<Entity> ents = doc.Editor.SelectAll().GetEntityByOK(OpenMode.ForWrite);
#pragma warning disable CS0618
                doc.Editor.SetImpliedSelection2006(ents.Select(x => x.Id).ToArray());
#pragma warning restore CS0618

                trans.Commit();
            }
        }

        [PopMenuItem(2), MenuMacro("2.   (全选)选中状态：原生api")]
        [AcadPopupMenuItem("2.   (全选)选中状态：原生api", 2)]
        [CommandMethod("SetImpliedSelection2")]
        public static void SetImpliedSelection2()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                IEnumerable<Entity> ents = doc.Editor.SelectAll().GetEntityByOK(OpenMode.ForWrite);
                doc.Editor.SetImpliedSelection(ents.Select(x => x.Id).ToArray());

                trans.Commit();
            }
        }

        [PopMenuItem(3), MenuMacro("3.   框选->原api")]
        [AcadPopupMenuItem("3.   框选->原api", 3)]
        [CommandMethod("WindowPolygon1")]
        public void WindowPolygon1()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                //框选
                Point3dCollection pts = doc.Editor.GetSquareSelect("第一个点", "第二个点");
                if (pts == null)
                    return;

                //原api
                PromptSelectionResult laypsr = doc.Editor.SelectWindowPolygon(pts);
                if (laypsr.Status != PromptStatus.OK)
                    return;
                int entCount = laypsr.Value.Count;

                doc.Editor.WriteMessage(string.Format("cad原生api得到实体{0}个。", entCount));

                trans.Commit();
            }
        }

        [PopMenuItem(4), MenuMacro("4.   框选->修改api")]
        [AcadPopupMenuItem("4.   框选->修改api", 4)]
        [CommandMethod("WindowPolygon2")]
        public void WindowPolygon2()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                //框选
                Point3dCollection pts = doc.Editor.GetSquareSelect("第一个点", "第二个点");
                if (pts == null)
                    return;

                //修改api
                doc.Editor.SetZoomWSynchronize(pts.Cast<Point3d>());
                int entsCount = doc.Editor.SelectWindowPolygon(new Point3dCollection(pts.Cast<Point3d>().ToArray())).GetEntityByOK(OpenMode.ForWrite).Count();
                doc.Editor.SetZoomPSynchronize();

                doc.Editor.WriteMessage(string.Format("修正的api得到实体{0}个.", entsCount));

                trans.Commit();
            }
        }

        [PopMenuItem(5), MenuMacro("5.   框选->SendStringToExecuteSynchronize api")]
        [AcadPopupMenuItem("5.   框选->SendStringToExecuteSynchronize api", 5)]
        [CommandMethod("WindowPolygon3")]
        public void WindowPolygon3()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                //框选
                Point3dCollection pts = doc.Editor.GetSquareSelect("第一个点", "第二个点");
                if (pts == null)
                    return;

                //修改api
                double v1 = pts.Cast<Point3d>().Min(x => x.X);
                double v2 = pts.Cast<Point3d>().Min(x => x.Y);
                double v3 = pts.Cast<Point3d>().Max(x => x.X);
                double v4 = pts.Cast<Point3d>().Max(x => x.Y);
                doc.SendStringToExecute($"zoom w {v1},{v2},0 {v3},{v4},0 ", true, false, false);
                doc.SendStringToExecuteSynchronizeMehtod(() =>
                {
                    using (Transaction trans1 = doc.TransactionManager.StartTransaction())
                    {
                        int entsCount = doc.Editor.SelectWindowPolygon(new Point3dCollection(pts.Cast<Point3d>().ToArray())).GetEntityByOK(OpenMode.ForWrite).Count();
                        doc.Editor.WriteMessage(string.Format("修正的api得到实体{0}个.", entsCount));
                    }
                });
                doc.SendStringToExecute("zoom p ", true, false, false);

                trans.Commit();
            }
        }
    }
}