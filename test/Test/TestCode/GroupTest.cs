﻿using System.Collections.Generic;
using System.Linq;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class GroupTest
    {
        [PopMenuItem(1), MenuMacro("1.   添加到组A")]
        [AcadPopupMenuItem("1.   添加到组A", 1)]
        [CommandMethod("AddNewGroup")]
        public void AddNewGroup()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                IEnumerable<Entity> ents = doc.Editor.SelectAll().GetEntityByOK(OpenMode.ForWrite);
                if (ents.Count() == 0)
                    return;

                doc.Database.AddSeriesNumGroup("A", ents.Select(x => x.Id).ToArray());

                trans.Commit();
            }
        }

        [PopMenuItem(2), MenuMacro("2.   得到组名")]
        [AcadPopupMenuItem("2.   得到组名", 2)]
        [CommandMethod("GetGroup")]
        public void GetGroup()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                //问题：无法像CAD原生命令`GROUPEDIT`一样选择组，只能选实体。
                Entity ent = doc.Editor.Get<Entity>("组中实体") as Entity;
                if (ent == null)
                    return;

                IEnumerable<Group> groups = ent.GetGroups(OpenMode.ForRead);
                if (groups.Count() != 0)
                {
                    doc.Editor.WriteMessage(groups.First().Name);
                }
                else
                {
                    doc.Editor.WriteMessage("无组！！！");
                }

                trans.Commit();
            }
        }

        [PopMenuItem(3), MenuMacro("3.   删除A组")]
        [AcadPopupMenuItem("3.   删除A组", 2)]
        [CommandMethod("RemoveGroup")]
        public void RemoveGroup()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            using (Transaction trans = doc.TransactionManager.StartTransaction())
            {
                doc.Database.RemoveGroup("A1");
                trans.Commit();
            }
        }
    }
}
