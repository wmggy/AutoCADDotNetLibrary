﻿using System.Windows.Forms;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Test
{
    public class TabExtension
    {
        [Initialize]
        public static void Show()
        {
            Application.DisplayingCustomizeDialog += new TabbedDialogEventHandler(Application_DisplayingCustomizeDialog);
            Application.DisplayingDraftingSettingsDialog += new TabbedDialogEventHandler(Application_DisplayingCustomizeDialog);
            Application.DisplayingOptionDialog += new TabbedDialogEventHandler(Application_DisplayingCustomizeDialog);
        }


        public static void Application_DisplayingCustomizeDialog(object sender, TabbedDialogEventArgs e)
        {
            Button b = new Button();
            b.Text = "点击";

            Control c = new Control();
            c.Controls.Add(b);

            e.AddTab("添加tab", new TabbedDialogExtension(c, () => MessageBox.Show("OK")));
        }
    }
}
