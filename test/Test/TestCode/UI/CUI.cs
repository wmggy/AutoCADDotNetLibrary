﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Customization;
using Autodesk.AutoCAD.Interop;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class CUI
    {
        public static readonly string CUIXFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\TestCUI.cuix";

        [Initialize(Index = -100)]//先执行
        public static void LoadCUI()
        {
            CustomizationSection acadCui = new CustomizationSection(AutoCADDotNetLibrary.CADPath.AcadCUIX);
#if !DEBUG
            bool r = acadCui.PartialCuiFiles.Contains(CUIXFilePath);
            if (r == true)
            {
                return;
            }
            if (!File.Exists(CUIXFilePath))
            {
                CreateCUI();
            }
#else
            CreateCUI();
#endif

            //未融合的情况
            acadCui.PartialCuiFiles.Remove(CUIXFilePath);
            acadCui.Save();

#if !AC2010
            //重新加载
            Application.UnloadPartialMenu(CUIXFilePath);
            Application.LoadPartialMenu(CUIXFilePath);
            Application.ReloadAllMenus();
#endif
        }

#if AC2010
        [IdleInitialize]
        public void Load()
        {
            CustomizationSection acadCui = new CustomizationSection(AutoCADDotNetLibrary.CADPath.AcadCUIX);
            bool r = acadCui.PartialCuiFiles.Contains(CUIXFilePath);
            if (r == true)
            {
                return;
            }
            Document doc = Application.DocumentManager.MdiActiveDocument;
            SystemVariable.SetSystemVariable(SystemVariableType.NOMUTT, 1);
            SystemVariable.SetSystemVariable(SystemVariableType.FILEDIA, 0);

            doc.SendStringToExecute("cuiload " + CUIXFilePath + " ", true, false, false);

            SystemVariable.RestoreSystemVariable(doc);
        }
#endif

        public static void CreateCUI()
        {
            CustomizationSection cs = new CustomizationSection();
            cs.MenuGroupName = "TestCUI";

            //宏
            MacroGroup mg = new MacroGroup("TestMacroGroup", cs.MenuGroup);
            Dictionary<string, MenuMacro> macros = mg.AddAssemblyMenuMacro(Assembly.GetExecutingAssembly());

            //下拉菜单
            PopMenu pm1 = new PopMenu("下拉菜单", new System.Collections.Specialized.StringCollection() { "别名1", }, "", cs.MenuGroup);//别名不能为空
            PopMenuItem pmi1 = new PopMenuItem(macros["l2"], "直线", pm1, -1);
            PopMenuItem pmi2 = new PopMenuItem(macros["pl2"], "多段线", pm1, -1);
            PopMenuItem pmi3 = new PopMenuItem(pm1);//分隔符

            //二级菜单
            PopMenu pm2 = new PopMenu("二级菜单", null, "", pm1.CustomizationSection.MenuGroup);
            pm2.AddPopMenuItem(macros, typeof(CUI));
            PopMenuRef menuRef = new PopMenuRef(pm2, pm1, -1);

            //第二菜单
            PopMenu pm3 = new PopMenu("CUI菜单", new System.Collections.Specialized.StringCollection() { "别名2" }, "", cs.MenuGroup);//别名不能为空,不能重复
#if !AC2010
            pm3.AddPopMenuItem(macros, typeof(Palette));
            pm3.AddPopMenuItem(macros, typeof(DialogCommand));
#endif
            pm3.AddPopMenu("二级菜单", macros, typeof(CUI)).AddPopMenu("三级菜单", macros, typeof(CUI));
            new PopMenuItem(pm3);//分隔符
            pm3.AddPopMenuItem(macros, typeof(AOP));
            pm3.AddPopMenu("ProgressMeter", macros, typeof(StatusBarProgress));
            pm3.AddPopMenu("Database", macros, typeof(DatabaseTest));
            pm3.AddPopMenu("Editor", macros, typeof(EditorTest));
            pm3.AddPopMenu("Jig", macros, typeof(Jig));
            pm3.AddPopMenu("Entity", macros, typeof(EntityTest));
            pm3.AddPopMenu("XData", macros, typeof(XData));
            pm3.AddPopMenu("Group", macros, typeof(GroupTest));
            pm3.AddPopMenu("Layer", macros, typeof(LayerTest));
            pm3.AddPopMenu("SendStringToExecute", macros, typeof(SendStringToExecuteSynchronizeTest));
            pm3.AddPopMenu("CAD命令", macros, typeof(TestCommandClass)).AddPopMenuItem(macros, typeof(CADCommand));
            pm3.AddPopMenu("注册dll", macros, typeof(Regedit));

            string dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            WindowTransformCUI.Transform(cs.MenuGroup.RibbonRoot, new MyRibbonTab(), macros, x =>
            {
                x.SmallImage = dir + "\\CUIImage\\S2.ico";
                x.LargeImage = dir + "\\CUIImage\\S1.ico";
            });
            cs.MenuGroup.RibbonRoot.RibbonTabSources[0].Text += "CUI";

            cs.SaveAs(CUIXFilePath);
        }

        #region DynamicMenuItemAtt

        [PopMenuItem(1, Name = "PopMenuItem"), MenuMacro("直线", UID = "l2", CLICommand = "cli_l2", HelpString = "help_l2", SmallImageFileName = "CUIImage/B1.ico", LargeImageFileName = "CUIImage/A1.ico")]
        [CommandMethod("l2")]
        public void l2()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }

        [PopMenuItem(2), MenuMacro("多段线", UID = "pl2", CLICommand = "cli_pl2", HelpString = "help_pl2", SmallImageFileName = "CUIImage/B1.ico", LargeImageFileName = "CUIImage/A1.ico")]
        [CommandMethod("pl2")]
        public void pl2()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("pl\n", true, false, false);
        }

        [PopMenuItem(3), MenuMacro("测试寻找Com菜单")]
        [CommandMethod("FindMenu")]
        public void FindMenu()
        {
            AcadApplication acadApp = (AcadApplication)Application.AcadApplication;

            AcadPopupMenuItem r1 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "TestCUI", "下拉菜单", "直线") as AcadPopupMenuItem;
            AcadPopupMenuItem r2 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "TestCUI", "下拉菜单", "二级菜单") as AcadPopupMenuItem;
            AcadPopupMenuItem r3 = AcadPopupMenuItemAtt.GetAcadPopupMenuItem(acadApp, "TestCUI", "下拉菜单", "二级菜单", "多段线") as AcadPopupMenuItem;

            bool result = r1.Label == "直线" && r2.Label == "二级菜单" && r3.Label == "多段线";
            Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage($"测试{result}");
        }

        #endregion
    }
}
