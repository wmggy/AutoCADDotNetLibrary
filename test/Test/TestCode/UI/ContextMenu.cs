﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;


namespace Test
{
    public class ContextMenu
    {
        [Initialize]
        public void ShowObjectContextMenu()
        {
            ContextMenuExtension cme = new ContextMenuExtension();
            MenuItem it = new MenuItem("快捷菜单实体");
            it.AddContextMenuItem(typeof(ContextMenu));
            cme.MenuItems.Add(it);

            RXClass rxc = Entity.GetClass(typeof(Entity));
            Application.AddObjectContextMenuExtension(rxc, cme);
        }

        [Initialize]
        public void ShowContextMenu()
        {
            ContextMenuExtension cme = new ContextMenuExtension();
            cme.Title = "快捷菜单";
            cme.AddContextMenuItem(typeof(ContextMenu));

            Application.AddDefaultContextMenuExtension(cme);
        }



        [ContextMenuItem("快捷菜单直线", 1, IconName = "Test.ContextMenuImage.直线.ico")]
        [CommandMethod("l1")]
        public void l1()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("l\n", true, false, false);
        }

        [ContextMenuItem("快捷菜单多段线", 2, IconName = "Test.ContextMenuImage.曲线.ico")]
        [CommandMethod("pl1")]
        public void pl1()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("pl\n", true, false, false);
        }

        #region 不建议使用，只是一种思路，目前感觉还有些问题

        //快捷菜单
        //[Initialize]
        //public void ShortcutMenu()
        //{
        //    ((AcadApplication)Application.AcadApplication).ActiveDocument.BeginShortcutMenuDefault += new _DAcadDocumentEvents_BeginShortcutMenuDefaultEventHandler(BeginShortcutMenuDefault);
        //}

        //public void BeginShortcutMenuDefault(out AcadPopupMenu menu)
        //{
        //    AcadApplication acadApp = (AcadApplication)Application.AcadApplication;
        //    var asd = acadApp.MenuGroups.OfType<AcadMenuGroup>().Select(x => x.Name).ToList();
        //    menu = acadApp.MenuGroups.OfType<AcadMenuGroup>().First(x => x.Name.ToLower() == "acad").Menus.OfType<AcadPopupMenu>().First(x => x.Name == "默认菜单");
        //    menu.AddMenuItem(menu.Count, "快捷菜单！！！", "\n");
        //    //acadApp.ActiveDocument.BeginShortcutMenuDefault -= new _DAcadDocumentEvents_BeginShortcutMenuDefaultEventHandler(BeginShortcutMenuDefault);
        //}

        #endregion
    }
}
