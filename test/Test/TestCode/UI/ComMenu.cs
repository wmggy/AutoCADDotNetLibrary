﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Interop;

namespace Test
{
    public class ComMenu
    {
        private static AcadPopupMenu _comMenu;

        [ComMenuInitialize]
        public void ShowMenu()
        {
            AcadApplication acadApp = (AcadApplication)Application.AcadApplication;
            //没有时，添加根菜单
            if (_comMenu == null)
            {
                _comMenu = acadApp.MenuGroups.Item(0).Menus.Add("COM菜单");

                //添加功能
#if !AC2010
                _comMenu.AddAcadPopupMenuItem(typeof(Palette));
                _comMenu.AddAcadPopupMenuItem(typeof(DialogCommand));
                _comMenu.AddSeparator(_comMenu.Count);
#endif
                _comMenu.AddAcadPopupMenuItem(typeof(AOP));
                _comMenu.AddSubMenu(_comMenu.Count, "ProgressMeter").AddAcadPopupMenuItem(typeof(StatusBarProgress));
                _comMenu.AddSubMenu(_comMenu.Count, "Database").AddAcadPopupMenuItem(typeof(DatabaseTest));
                _comMenu.AddSubMenu(_comMenu.Count, "Editor").AddAcadPopupMenuItem(typeof(EditorTest));
                _comMenu.AddSubMenu(_comMenu.Count, "Jig").AddAcadPopupMenuItem(typeof(Jig));
                _comMenu.AddSubMenu(_comMenu.Count, "Entity").AddAcadPopupMenuItem(typeof(EntityTest));
                _comMenu.AddSubMenu(_comMenu.Count, "XData").AddAcadPopupMenuItem(typeof(XData));
                _comMenu.AddSubMenu(_comMenu.Count, "Group").AddAcadPopupMenuItem(typeof(GroupTest));
                _comMenu.AddSubMenu(_comMenu.Count, "Layer").AddAcadPopupMenuItem(typeof(LayerTest));
                _comMenu.AddSubMenu(_comMenu.Count, "SendStringToExecute").AddAcadPopupMenuItem(typeof(SendStringToExecuteSynchronizeTest));
                AcadPopupMenu menuItem1 = _comMenu.AddSubMenu(_comMenu.Count, "CAD命令");
                menuItem1.AddAcadPopupMenuItem(typeof(TestCommandClass));
                menuItem1.AddAcadPopupMenuItem(typeof(CADCommand));
                _comMenu.AddSubMenu(_comMenu.Count, "注册dll").AddAcadPopupMenuItem(typeof(Regedit));
            }
            //将定义的菜单显示在AutoCAD菜单栏的最后
            if (!_comMenu.OnMenuBar)
            {
                _comMenu.InsertInMenuBar(acadApp.MenuBar.Count);
            }
        }
    }
}
