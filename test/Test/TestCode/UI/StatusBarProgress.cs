﻿using AutoCADDotNetLibrary;
using Autodesk.AutoCAD.Internal;
using Autodesk.AutoCAD.Runtime;

namespace Test
{
    public class StatusBarProgress
    {
        [PopMenuItem(1), MenuMacro("1.   设置进度条(Utils)")]
        [AcadPopupMenuItem("1.   设置进度条(Utils)", 1)]
        [CommandMethod("SetStatusBarProgressMeter2")]
        public void SetStatusBarProgressMeter2()
        {
            Utils.SetApplicationStatusBarProgressMeter("hello ProgressBar", 0, 100);
            for (int i = 0; i <= 100; i++)
            {
                System.Threading.Thread.Sleep(100);
                Utils.SetApplicationStatusBarProgressMeter(i);
            }
            Utils.RestoreApplicationStatusBar();
        }

#if AC2012
        [PopMenuItem(2), MenuMacro("2.   设置进度条(PInvoke)")]
        [AcadPopupMenuItem("2.   设置进度条(PInvoke)", 2)]
        [CommandMethod("SetStatusBarProgressMeter1")]
        public void SetStatusBarProgressMeter1()
        {
            StatusBarProgressMeter.SetStatusBarProgressMeter("hello ProgressBar", 0, 100);
            for (int i = 0; i <= 100; i++)
            {
                System.Threading.Thread.Sleep(100);
                StatusBarProgressMeter.SetStatusBarProgressMeterPos(i);
            }
            StatusBarProgressMeter.RestoreStatusBar();
        }
#endif
    }


#if AC2012
    /// <summary>
    /// 进度条
    /// </summary>
    public static class StatusBarProgressMeter
    {
        /// <summary>
        /// 设置进度条
        /// </summary>
        /// <param name="pszLabel">进度表的标签</param>
        /// <param name="nMinPos">进度表的最小位置</param>
        /// <param name="nMaxPos">进度表的最大位置</param>
        /// <returns>0，成功；-1，失败</returns>
        [DllImport("acad.exe", EntryPoint = "?acedSetStatusBarProgressMeter@@YAHPEB_WHH@Z", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetStatusBarProgressMeter([MarshalAs(UnmanagedType.LPWStr)] string pszLabel, int nMinPos, int nMaxPos);

        /// <summary>
        /// 设置状态栏的当前位置
        /// </summary>
        /// <param name="nPos">设置范围内的正值可设置当前位置，范围内的负值可向当前位置添加一个量</param>
        /// <returns>0，成功；-1，失败</returns>
        [DllImport("acad.exe", EntryPoint = "?acedSetStatusBarProgressMeterPos@@YAHH@Z", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetStatusBarProgressMeterPos(int nPos);

        /// <summary>
        /// 恢复AutoCAD的原始状态栏
        /// </summary>
        [DllImport("acad.exe", EntryPoint = "?acedRestoreStatusBar@@YAXXZ", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern void RestoreStatusBar();
    }
#endif
}
